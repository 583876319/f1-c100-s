#ifndef __SYS_LCD_H__
#define __SYS_LCD_H__

#include "sys_types.h"

/*******************************16位常用色定义****************/
#define WHITE                   0xFFFF
#define BLACK                   0x0000
#define BLUE                    0x001F                            // 蓝
#define BRED                    0XF81F
#define GRED                    0XFFE0
#define GBLUE                   0X07FF
#define RED                     0xF800
#define MAGENTA                 0xF81F
#define GREEN                   0x07E0
#define CYAN                    0x7FFF
#define YELLOW                  0xFFE0
#define BROWN                   0XBC40                            // 棕色
#define BRRED                   0XFC07                            // 棕红色
#define GRAY                    0X8430                            // 灰色
#define DARKBLUE                0X01CF                            // 深蓝色
#define LIGHTBLUE               0X7D7C                            // 浅蓝色
#define GRAYBLUE                0X5458                            // 灰蓝色
/********************************************************/

#define TCON_Base_Address       (u32_t)0x01c0c000
#define CCU_Base_Address        (u32_t)0x01C20000
#define CCU_TCON_CLK_REG        (u32_t) CCU_Base_Address + 0x0118
#define CCU_BUS_SOFT_RST_REG0   (u32_t) CCU_Base_Address + 0x02C0
#define CCU_BUS_SOFT_RST_REG1   (u32_t) CCU_Base_Address + 0x02C4
#define CCU_BUS_CLK_GATING_REG0 (u32_t) CCU_Base_Address + 0x0060
#define CCU_BUS_CLK_GATING_REG1 (u32_t) CCU_Base_Address + 0x0064
#define DMA_Base_Address        (u32_t)0x01C02000
#define DEBE_Base_Address       (u32_t)0x01E60000
#define DEFE_Base_Address       (u32_t)0x01E00000

struct __lcd_timing
{
	int mode; // LCD模式 0=cpu 1-rgb565 2=rgb666 3=rgb888 4=rgb8位串行 5=ccir656
	int width;
	int h_front_porch;
	int h_back_porch;
	int h_sync_len;
	int height;
	int v_front_porch;
	int v_back_porch;
	int v_sync_len;
	int h_sync_active;
	int v_sync_active;
	int den_active;
	int clk_active;
	/*1=并行,2=2串行,3=3串行*/
	int serial;
	// 0: 18bit/256K mode
	// 1: 16bit mode0
	// 2: 16bit mode1
	// 3: 16bit mode2
	// 4: 16bit mode3
	// 5: 9bit mode
	// 6: 8bit 256K mode
	// 7: 8bit 65K mode
	int cpu_mode;
	int n;
	int m;
	int f;
};

struct fb_f1c100s_pdata_t
{
	virtual_addr_t virtdefe;
	virtual_addr_t virtdebe;
	virtual_addr_t virttcon;
	int width;
	int height;
	int bits_per_pixel;
	void *vram[2];
};

extern struct fb_f1c100s_pdata_t *lcd_pdat;
void TCON_ISR(void);
void Sys_LCD_Init(int width, int height, unsigned int *buff1, unsigned int *buff2);
void f1c100s_debe_set_address(struct fb_f1c100s_pdata_t *pdat, void *vram);
void f1c100s_tcon_enable(struct fb_f1c100s_pdata_t *pdat);
void f1c100s_tcon_disable(struct fb_f1c100s_pdata_t *pdat);
//------------------------------------------------------------
void Tcon_Clk_Close(void);
void Tcon_Clk_Open(void);
void Debe_Clk_Open(void);
void Debe_Clk_Close(void);
//------------------------------------------------------------
void Set_LayerX_window(int Layer, int x, int y, int w, int h);
void Enable_LayerX_Video(int Layer);
void Disable_LayerX_Video(int Layer);
//------------------------------------------------------------
void set_video_window(int x, int y, int w, int h);
void Enable_Layer_Video(void);
void Disable_Layer_Video(void);
//------------------------------------------------------------
void Set_Layer_format(int Layer, int format, int stride);

#endif
