#ifndef __SYS_SPI_H__
#define __SYS_SPI_H__

#define SPI0                   0    // 0x01C05000
#define SPI1                   1    // 0x01C06000

// ָ���
#define W25X_WriteEnable       0x06
#define W25X_WriteDisable      0x04

#define W25X_ReadStatusReg     0x05
#define W25X_ReadStatusReg1    0x05
#define W25X_ReadStatusReg2    0x35
#define W25X_ReadStatusReg3    0x15

#define W25X_WriteStatusReg    0x01
#define W25X_ReadData          0x03
#define W25X_ReadData_4ADDR    0X13
#define W25X_FastReadData      0x0B
#define W25X_FastReadDual      0x3B
#define W25X_PageProgram       0x02
#define W25X_PageProgram_4ADDR 0X12
// #define W25X_BlockErase			0xD8
#define W25X_SectorErase       0xd8
#define W25X_ChipErase         0xC7
#define W25X_PowerDown         0xB9
#define W25X_ReleasePowerDown  0xAB
#define W25X_DeviceID          0xAB
#define W25X_ManufactDeviceID  0x90
#define W25X_JedecDeviceID     0x9F

#define W25X_FastRead_QUAD     0x6B
#define W25X_PageProgram_QUAD  0x32

void W25Q128_Demo(void);
void spi_flash_init(int SPI);
int sys_spi_flash_read(int SPI, int addr, void *buf, int count);
void SPI_Flash_Write(int SPI, unsigned char *pBuffer, unsigned int WriteAddr, unsigned int NumByteToWrite);
void set_max_clk(int clk);
//===============================================
int SPI_Flash_Erase_Block32K(int SPI, int addr);
int SPI_Flash_Erase_Block64K(int SPI, int addr);
int SPI_Flash_Read_UID(int SPI, unsigned char *rbuff);
unsigned char spi_nor_flash_read_device_id(int SPI);
//===============================================
void sys_spi_select(int SPI);
void sys_spi_deselect(int SPI);
int sys_spi_transfer(int SPI, void *txbuf, void *rxbuf, int len);

#endif
