#include "sys_uart.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_lcd.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_timer.h"
#include <string.h>
#include <stdio.h>
#include <sys_mmu.h>
#include "stdlib.h"
//-----------------------
#include "ILI9342.h"
//-----------------------
#include "sys_lcd_conf.h"
//-----------------------BMP图
#include "test_bmp_rgb565_m.h"
#include "test_bmp_rgb888_m.h"
//-----------------------
#include "test_bmp_rgb565.h"
#include "test_bmp_rgb888.h"
#include "test_bmp_argb8888.h"
//-----------------------点陈图
#include "test_image_rgb565.h"
#include "test_image_rgb888.h"
//-----------------------
#include "gui_draw.h"
#include "gui_draw_bmp.h"
#include "gui_draw_rgb565.h"
#include "gui_draw_argb8888.h"
//-----------------------
__align(4) unsigned int LCDbuff[XSIZE_PHYS * YSIZE_PHYS]; // 显存

/*---------------------------------------------------
RGB565测试
---------------------------------------------------*/
void GUI_Draw_RGB565_Test(void)
{
	// 设置画布
	Set_Canvas((unsigned int *)LCDbuff, XSIZE_PHYS, YSIZE_PHYS, CMODE_RGB565);
	// 显示BMP格式图片
	GUI_Draw_bmp(0, 0, test_bmp_rgb565_m);
	// 4个角的图片
	GUI_Draw_bmp(0, 0, test_bmp_rgb565);
	GUI_Draw_bmp(XSIZE_PHYS - 51, 0, test_bmp_rgb565);
	GUI_Draw_bmp(0, YSIZE_PHYS - 45, test_bmp_rgb565);
	GUI_Draw_bmp(XSIZE_PHYS - 51, YSIZE_PHYS - 45, test_bmp_rgb565);
	delay_ms(1500);
	// 清屏
	GUI_Clear_RGB565(RGB565_WHITE);
	// String
	GUI_Draw_String_RGB565(10, 10, "F1C100S", RGB565_RED, 0, 0, 1);
	GUI_Draw_String_RGB565(10, 26, "F1C100S", RGB565_GREEN, RGB565_BLACK, 1, 2);
	GUI_Draw_String_RGB565(10, 58, "F1C100S", RGB565_BLUE, 0, 0, 3);
#if (!defined LCD_TYPE_MCU_320_240)
	// 汉字
	for (int i = 0; i < 3; i++)
	{
		GUI_Draw_HZ2424_RGB565(200 + i * 24, 10, i, RGB565_RED, 0, 0, 1);
		GUI_Draw_HZ2424_RGB565(200 + i * 48, 34, i, RGB565_GREEN, RGB565_BLACK, 1, 2);
		GUI_Draw_HZ2424_RGB565(200 + i * 96, 82, i, RGB565_BLUE, 0, 0, 3);
	}
	// 画矩形-填充
	GUI_Draw_Rectangle_RGB565(10, 112, 40, 40, RGB565_RED, 1, 0);
	GUI_Draw_Rectangle_RGB565(50, 112, 40, 40, RGB565_GREEN, 1, 0);
	GUI_Draw_Rectangle_RGB565(90, 112, 40, 40, RGB565_BLUE, 1, 0);
	// 画矩形-不填充
	GUI_Draw_Rectangle_RGB565(330, 166, 40, 40, RGB565_RED, 0, 1);
	GUI_Draw_Rectangle_RGB565(370, 166, 40, 40, RGB565_GREEN, 0, 2);
	GUI_Draw_Rectangle_RGB565(410, 166, 40, 40, RGB565_BLUE, 0, 3);
	// 画图片
	GUI_Draw_Picture_RGB565(10, 165, 156, 100, (char *)test_image_rgb565);
	// 画直线
	GUI_Draw_Line_RGB565(180, 166, 240, 166, RGB565_RED, 1);
	GUI_Draw_Line_RGB565(180, 176, 240, 176, RGB565_GREEN, 2);
	GUI_Draw_Line_RGB565(180, 186, 240, 186, RGB565_BLUE, 3);
	GUI_Draw_Line_RGB565(180, 196, 180, 256, RGB565_RED, 1);
	GUI_Draw_Line_RGB565(190, 196, 190, 256, RGB565_GREEN, 2);
	GUI_Draw_Line_RGB565(200, 196, 200, 256, RGB565_BLUE, 3);
	// 画斜线
	GUI_Draw_Line_RGB565(250, 166, 310, 206, RGB565_RED, 1);
	GUI_Draw_Line_RGB565(250, 176, 310, 216, RGB565_GREEN, 2);
	GUI_Draw_Line_RGB565(250, 186, 310, 226, RGB565_BLUE, 3);
	// 画圆
	GUI_Draw_Circle_RGB565(370, 40, 6, RGB565_RED, 1, 0);
	GUI_Draw_Circle_RGB565(397, 40, 12, RGB565_RED, 1, 0);
	//--
	GUI_Draw_Circle_RGB565(440, 40, 5, RGB565_RED, 0, 1);
	GUI_Draw_Circle_RGB565(440, 40, 10, RGB565_GREEN, 0, 2);
	GUI_Draw_Circle_RGB565(440, 40, 15, RGB565_BLUE, 0, 3);
	GUI_Draw_Circle_RGB565(440, 40, 21, RGB565_RED, 0, 4);
	// 循环刷色块
	while (1)
	{
		sysprintf("RUN\r\n");
		GUI_Draw_Rectangle_RGB565(330, 230, 120, 30, RGB565_RED, 1, 0);
		delay_ms(500);
		GUI_Draw_Rectangle_RGB565(330, 230, 120, 30, RGB565_GREEN, 1, 0);
		delay_ms(500);
		GUI_Draw_Rectangle_RGB565(330, 230, 120, 30, RGB565_BLUE, 1, 0);
		delay_ms(500);
	}
#else
	while (1)
	{
		sysprintf("RUN\r\n");
		delay_ms(500);
	}
#endif
}
/*---------------------------------------------------
ARGB8888测试
---------------------------------------------------*/
void GUI_Draw_ARGB8888_Test(void)
{
	// 设置画布
	Set_Canvas((unsigned int *)LCDbuff, XSIZE_PHYS, YSIZE_PHYS, CMODE_ARGB8888);
	// 显示BMP格式图片
	GUI_Draw_bmp(0, 0, test_bmp_rgb888_m);
	// 4个角的图片
	GUI_Draw_bmp(0, 0, test_bmp_rgb888);
	GUI_Draw_bmp(XSIZE_PHYS - 51, 0, test_bmp_rgb888);
	GUI_Draw_bmp(0, YSIZE_PHYS - 45, test_bmp_rgb888);
	GUI_Draw_bmp(XSIZE_PHYS - 51, YSIZE_PHYS - 45, test_bmp_rgb888);
	delay_ms(1500);
	// 清屏
	GUI_Clear_ARGB8888(ARGB8888_WHITE);
	// String
	GUI_Draw_String_ARGB8888(10, 10, "F1C100S", ARGB8888_RED, 0, 0, 1);
	GUI_Draw_String_ARGB8888(10, 26, "F1C100S", ARGB8888_GREEN, ARGB8888_BLACK, 1, 2);
	GUI_Draw_String_ARGB8888(10, 58, "F1C100S", ARGB8888_BLUE, 0, 0, 3);
#if (!defined LCD_TYPE_MCU_320_240)
	// 汉字
	for (int i = 0; i < 3; i++)
	{
		GUI_Draw_HZ2424_ARGB8888(200 + i * 24, 10, i, ARGB8888_RED, 0, 0, 1);
		GUI_Draw_HZ2424_ARGB8888(200 + i * 48, 34, i, ARGB8888_GREEN, ARGB8888_BLACK, 1, 2);
		GUI_Draw_HZ2424_ARGB8888(200 + i * 96, 82, i, ARGB8888_BLUE, 0, 0, 3);
	}
	// 画矩形-填充
	GUI_Draw_Rectangle_ARGB8888(10, 112, 40, 40, ARGB8888_RED, 1, 0);
	GUI_Draw_Rectangle_ARGB8888(50, 112, 40, 40, ARGB8888_GREEN, 1, 0);
	GUI_Draw_Rectangle_ARGB8888(90, 112, 40, 40, ARGB8888_BLUE, 1, 0);
	// 画矩形-不填充
	GUI_Draw_Rectangle_ARGB8888(330, 166, 40, 40, ARGB8888_RED, 0, 1);
	GUI_Draw_Rectangle_ARGB8888(370, 166, 40, 40, ARGB8888_GREEN, 0, 2);
	GUI_Draw_Rectangle_ARGB8888(410, 166, 40, 40, ARGB8888_BLUE, 0, 3);
	// 画图片
	GUI_Draw_Picture_ARGB8888(10, 165, 156, 100, (char *)test_image_rgb888);
	// 画直线
	GUI_Draw_Line_ARGB8888(180, 166, 240, 166, ARGB8888_RED, 1);
	GUI_Draw_Line_ARGB8888(180, 176, 240, 176, ARGB8888_GREEN, 2);
	GUI_Draw_Line_ARGB8888(180, 186, 240, 186, ARGB8888_BLUE, 3);
	GUI_Draw_Line_ARGB8888(180, 196, 180, 256, ARGB8888_RED, 1);
	GUI_Draw_Line_ARGB8888(190, 196, 190, 256, ARGB8888_GREEN, 2);
	GUI_Draw_Line_ARGB8888(200, 196, 200, 256, ARGB8888_BLUE, 3);
	// 画斜线
	GUI_Draw_Line_ARGB8888(250, 166, 310, 206, ARGB8888_RED, 1);
	GUI_Draw_Line_ARGB8888(250, 176, 310, 216, ARGB8888_GREEN, 2);
	GUI_Draw_Line_ARGB8888(250, 186, 310, 226, ARGB8888_BLUE, 3);
	// 画圆
	GUI_Draw_Circle_ARGB8888(370, 40, 6, ARGB8888_RED, 1, 0);
	GUI_Draw_Circle_ARGB8888(397, 40, 12, ARGB8888_RED, 1, 0);
	//--
	GUI_Draw_Circle_ARGB8888(440, 40, 5, ARGB8888_RED, 0, 1);
	GUI_Draw_Circle_ARGB8888(440, 40, 10, ARGB8888_GREEN, 0, 2);
	GUI_Draw_Circle_ARGB8888(440, 40, 15, ARGB8888_BLUE, 0, 3);
	GUI_Draw_Circle_ARGB8888(440, 40, 21, ARGB8888_RED, 0, 4);
	// 循环刷色块
	while (1)
	{
		sysprintf("RUN\r\n");
		GUI_Draw_Rectangle_ARGB8888(330, 230, 120, 30, ARGB8888_RED, 1, 0);
		delay_ms(500);
		GUI_Draw_Rectangle_ARGB8888(330, 230, 120, 30, ARGB8888_GREEN, 1, 0);
		delay_ms(500);
		GUI_Draw_Rectangle_ARGB8888(330, 230, 120, 30, ARGB8888_BLUE, 1, 0);
		delay_ms(500);
	}
#else
	while (1)
	{
		sysprintf("RUN\r\n");
		delay_ms(500);
	}
#endif
}
/*---------------------------------------------------
---main
---main
---------------------------------------------------*/
int main(void)
{
	unsigned int CPU_Frequency_hz = 408 * 1000000; // HZ
	Sys_Clock_Init(CPU_Frequency_hz);
	sys_mmu_init();
	Sys_Uart_Init(UART0, CPU_Frequency_hz, 115200, 0);

	sysprintf("\r\n\r\n\r\n");
	sysprintf("Sys_Start...\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);
	for (int i = 0; i <= 21; i++)
	{
		GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_IN, GPIO_PuPd_NOPULL);
	}
	delay_ms(10);

	GPIO_Congif(GPIOE, GPIO_Pin_5, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);
	GPIO_Congif(GPIOE, GPIO_Pin_6, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);
	GPIO_Congif(GPIOE, GPIO_Pin_11, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);

	GPIO_SET(GPIOE, GPIO_Pin_5);
	GPIO_SET(GPIOE, GPIO_Pin_6);
	GPIO_SET(GPIOE, GPIO_Pin_11);

	/*初始化LCD*/
#ifdef LCD_TYPE_MCU_320_240
	ili9342_init();
#endif

	Sys_LCD_Init(XSIZE_PHYS, YSIZE_PHYS, (unsigned int *)LCDbuff, NULL);

#if 1 /*RGB565测试*/
	GUI_Draw_RGB565_Test();
#else /*ARGB8888测试*/
	Set_Layer_format(0, 0xA, 5); // 设置LCD为ARGB8888格式
	GUI_Draw_ARGB8888_Test();
#endif
}
