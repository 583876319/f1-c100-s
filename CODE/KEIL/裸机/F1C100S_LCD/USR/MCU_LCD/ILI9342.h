#ifndef __ILI9342_H__
#define __ILI9342_H__

#include "sys_lcd_conf.h"

#ifdef LCD_TYPE_MCU_320_240
#define LCD_WS XSIZE_PHYS
#define LCD_HS YSIZE_PHYS
#else
#define LCD_WS 0
#define LCD_HS 0
#endif

#define RGB888TO565(R, G, B) (((R >> 3) << 11) | ((G >> 2) << 5) | ((B >> 3) << 0))

void ili9342_init(void);
void lcd_clear(unsigned short color);
void lcd_picture(unsigned char *buf);
void lcd_bled_ctrl(int en);
void lcd_draw_rectangle(int x, int y, int w, int h, unsigned short color);
void lcd_write_dot(int x, int y, unsigned short color);
unsigned short lcd_read_dot(int x, int y);
void LCD_SetFange(unsigned short start_x, unsigned short end_x, unsigned short start_y, unsigned short end_y);

#endif
