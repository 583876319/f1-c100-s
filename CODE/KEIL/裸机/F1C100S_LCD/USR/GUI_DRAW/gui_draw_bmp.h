#ifndef __LCD_DRAW_H__
#define __LCD_DRAW_H__

typedef struct tagBITMAPFILEHEADER
{
  //  unsigned short bfType;   // 19778，必须是BM字符串，对应的十六进制为0x4d42,十进制为19778
  unsigned int bfSize;        // 文件大小
  unsigned short bfReserved1; // 一般为0
  unsigned short bfReserved2; // 一般为0
  unsigned int bfOffBits;     // 从文件头到像素数据的偏移，也就是这两
} BITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER
{
  unsigned int biSize;         // 此结构体的大小
  int biWidth;                 // 图像的宽
  int biHeight;                // 图像的高
  unsigned short biPlanes;     // 1
  unsigned short biBitCount;   // 一像素所占的位数，一般为24
  unsigned int biCompression;  // 0
  unsigned int biSizeImage;    // 像素数据所占大小, 这个值应该等于上面文件头结构中bfSize-bfOffBits
  int biXPelsPerMeter;         // 0
  int biYPelsPerMeter;         // 0
  unsigned int biClrUsed;      // 0
  unsigned int biClrImportant; // 0
} BITMAPINFOHEADER;

int GUI_Draw_bmp(int x, int y, unsigned char *buff);

#endif
