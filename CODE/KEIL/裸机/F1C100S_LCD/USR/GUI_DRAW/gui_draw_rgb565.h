#ifndef __GUI_DRAW_RGB565_H__
#define __GUI_DRAW_RGB565_H__

// 常用颜色
#define RGB565_BLACK          0x0000
#define RGB565_WHITE          0xFFFF
#define RGB565_GREEN          0x07E0
#define RGB565_RED            0XF800
#define RGB565_GREEN          0x07E0
#define RGB565_BLUE           0x001F

// 常用函数
void GUI_Clear_RGB565(unsigned short Color);
void GUI_Draw_ASCII_RGB565(int x, int y, int n, unsigned short Color1, unsigned short Color2, int fx, int fd);
void GUI_Draw_String_RGB565(int x, int y, char *str, unsigned short Color1, unsigned short Color2, int fx, int fd);
void GUI_Draw_Picture_RGB565(int x, int y, int w, int h, char *pic);
void GUI_Draw_HZ1616_RGB565(int x, int y, int n, unsigned short Color1, unsigned short Color2, int fx, int fd);
void GUI_Draw_HZ2424_RGB565(int x, int y, int n, unsigned short Color1, unsigned short Color2, int fx, int fd);
void GUI_Draw_HZ3232_RGB565(int x, int y, int n, unsigned short Color1, unsigned short Color2, int fx, int fd);
void GUI_Draw_HZ4040_RGB565(int x, int y, int n, unsigned short Color1, unsigned short Color2, int fx, int fd);
void GUI_Draw_HZ4848_RGB565(int x, int y, int n, unsigned short Color1, unsigned short Color2, int fx, int fd);
void GUI_Move_Draw_Picture_RGB565(int x, int y, int w, int h, char *pic);
int GUI_ReadTF_Draw_Picture_RGB565(int x, int y, char *path);
void GUI_Draw_Rectangle_RGB565_X(int x, int y, int w, int h, unsigned short Color);
void GUI_Draw_Picture24_RGB565(int x, int y, int w, int h, char *pic);
void GUI_Draw_Picture32_RGB565(int x, int y, int w, int h, char *pic);
void GUI_Draw_HZ_RGB565(int x, int y, int v, unsigned short Color1, unsigned short Color2, int fx, int fd, char *hzprt);
void GUI_Draw_Line_RGB565(int x0, int y0, int x1, int y1, unsigned short color, int fd);
void GUI_Draw_Rectangle_RGB565(int x, int y, int w, int h, unsigned short Color, int fill, int line_w);
void GUI_Draw_Circle_RGB565(int cx, int cy, int r, unsigned short color, int fill, int line_w);

#endif
