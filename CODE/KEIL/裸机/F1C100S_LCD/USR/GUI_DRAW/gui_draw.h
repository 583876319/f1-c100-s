#ifndef __GUI_DRAW_H__
#define __GUI_DRAW_H__

#define CMODE_RGB565 1
#define CMODE_ARGB8888 2

extern int Canvas_w;
extern int Canvas_h;
extern int Canvas_len;
extern unsigned int *Canvas_argb8888;
extern unsigned short *Canvas_rgb565;
extern unsigned int canvas_mode;

// 设置图层
int Set_Canvas(unsigned int *canvas, int w, int h, int Canvas_mode);

#endif
