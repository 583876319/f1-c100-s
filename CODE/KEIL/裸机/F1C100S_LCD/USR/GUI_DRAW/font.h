#ifndef __FONT_H__
#define __FONT_H__

extern const char ASCII_1608[][16];
extern const char HZ_1616[][32];
extern const char HZ_2424[][24 * 24 / 8];
extern const char HZ_3232[][32 * 32 / 8];
extern const char HZ_4040[][40 * 40 / 8];
extern const char HZ_4848[][48 * 48 / 8];

#endif
