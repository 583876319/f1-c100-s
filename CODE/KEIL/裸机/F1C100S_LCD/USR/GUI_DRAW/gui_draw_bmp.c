#include "gui_draw_bmp.h"
#include "gui_draw.h"
#include "sys_uart.h"
#include "math.h"
#include "stdlib.h"
/*---------------------------------------------------
图片转换方法：
	1.使用PS打开图片,存储为BMP格式,保存时高级选项设置16位RGB565的BMP格式,翻转行序选上。
	2.然后使用Bin2C.exe转换成C文件，加入到工程就。
	3.图片数据需要4字节对齐, __align(4)。

带透明bmp图片制作说明：透明通道只有0xFF与0x00两个值
	1.打开PS-CS5,打开图片;
	2.使用魔术棒选取不需要透明的区域,右键->存储选区->默认设置.确定;
	3.打开通道,双击刚存储的选区通道,通道选项->选择被蒙版区域,不透明度设置为0,确定。
	4.存储为32位BMP图片,存储选项高级设置为ARGB8888,翻转行序。
	5.使用Bin2C.exe转换为C数组,加入工程调用就可以了。
----------------------------------------------------*/

/*
RGB565写入RGB565缓存
*/
void GUI_Draw_RGB565(int x, int y, int w, int h, unsigned char *pic)
{
	int i, l, a;
	// 判断是否为4的倍数
	int len = w * 2; // 图像一行的数据长度
	if ((len % 4) != 0)
		len = (len / 4 + 1) * 4; // 数据为非4的倍数
	// 显示
	for (i = 0; i < h; i++) // y
	{
		a = (y + i) * Canvas_w + x;
		for (l = 0; l < w; l++)
			Canvas_rgb565[a + l] = (pic[l * 2 + 1] << 8) | (pic[l * 2 + 0] << 0);
		pic += len; // 指向下行数据
	}
}
/*
RGB888写入ARGB8888缓存
*/
void GUI_Draw_RGB888(int x, int y, int w, int h, unsigned char *pic)
{
	int i, l, a;
	// 判断是否为4的倍数
	int len = w * 3; // 图像一行的数据长度
	if ((len % 4) != 0)
		len = (len / 4 + 1) * 4; // 数据为非4的倍数
	// 显示
	for (i = 0; i < h; i++) // y
	{
		a = (y + i) * Canvas_w + x;
		for (l = 0; l < w; l++)
			Canvas_argb8888[a + l] = 0xff000000 | (pic[l * 3 + 2] << 16) | (pic[l * 3 + 1] << 8) | (pic[l * 3 + 0] << 0);
		pic += len; // 指向下行数据
	}
}
/*
ARGB8888写入ARGB8888缓存
*/
void GUI_Draw_ARGB8888(int x, int y, int w, int h, unsigned char *pic)
{
	int i, l, a;
	// 判断是否为4的倍数
	int len = w * 4; // 图像一行的数据长度
	// 显示
	for (i = 0; i < h; i++) // y
	{
		a = (y + i) * Canvas_w + x;
		for (l = 0; l < w; l++)
			Canvas_argb8888[a + l] = (pic[l * 4 + 3] << 24) | (pic[l * 4 + 2] << 16) | (pic[l * 4 + 1] << 8) | (pic[l * 4 + 0] << 0);
		pic += len; // 指向下行数据
	}
}
/*
显示bmp
*/
int GUI_Draw_bmp(int x, int y, unsigned char *buff)
{
	int res = 1, i = 0;
	__align(4) unsigned char buff0[12];
	__align(4) unsigned char buff1[40];
	int w, h;
	BITMAPFILEHEADER *BitMapFileHeader;
	BITMAPINFOHEADER *BitMapInfoHeader;
	/*--对齐检查--*/
	if (((unsigned int)buff % 4) != 0)
	{
		sysprintf("ERR:The input address is not 4-Byte aligned!!!\r\n");
		return -1;
	}
	if (res == 1)
	{
		if ((buff[0] == 0x42) & (buff[1] == 0x4D))
		{
			for (i = 0; i < 12; i++)
				buff0[i] = buff[2 + i]; // 读文件头
			for (i = 0; i < 40; i++)
				buff1[i] = buff[14 + i]; // 读文件信息

			BitMapFileHeader = (BITMAPFILEHEADER *)(&buff0[0]);
			BitMapInfoHeader = (BITMAPINFOHEADER *)(&buff1[0]);
			sysprintf("bmp file size: %d \r\n", BitMapFileHeader->bfSize);
			sysprintf("bmp data bfOffBits: %d \r\n", BitMapFileHeader->bfOffBits);
			sysprintf("bmp w: %d \r\n", BitMapInfoHeader->biWidth);
			sysprintf("bmp h: %d \r\n", BitMapInfoHeader->biHeight);
			sysprintf("bmp biBit: %d \r\n", BitMapInfoHeader->biBitCount);
			w = BitMapInfoHeader->biWidth;
			h = BitMapInfoHeader->biHeight;
			switch (BitMapInfoHeader->biBitCount)
			{
			case 16:
			{
				if (canvas_mode != CMODE_RGB565)
				{
					sysprintf("ERR:canvas_mode\r\n");
					return -1;
				}
				GUI_Draw_RGB565(x, y, w, abs(h), &buff[BitMapFileHeader->bfOffBits]);
				break;
			}
			case 24:
			{
				if (canvas_mode != CMODE_ARGB8888)
				{
					sysprintf("ERR:canvas_mode\r\n");
					return -1;
				}
				GUI_Draw_RGB888(x, y, w, abs(h), &buff[BitMapFileHeader->bfOffBits]);
				break;
			}
			case 32:
			{
				if (canvas_mode != CMODE_ARGB8888)
				{
					sysprintf("ERR:canvas_mode\r\n");
					return -1;
				}
				GUI_Draw_ARGB8888(x, y, w, abs(h), &buff[BitMapFileHeader->bfOffBits]);
				break;
			}
			default:
				sysprintf("ERR:BitCount not supported[%d]\r\n", BitMapInfoHeader->biBitCount);
				return -1;
			}
		}
		else
		{
			sysprintf("ERR:bmp file type err! no BM type!\r\n");
			return -1;
		}
	}
	return 0;
}
