#ifndef __SYS_LCD_GUI_ARGB32_H__
#define __SYS_LCD_GUI_ARGB32_H__

// 常用颜色
#define ARGB8888_BLACK           0xFF000000
#define ARGB8888_WHITE           0xFFFFFFFF
#define ARGB8888_RED             0xFFFF0000
#define ARGB8888_GREEN           0xFF00FF00
#define ARGB8888_BLUE            0xFF0000FF

// 常用函数
void GUI_Clear_ARGB8888(unsigned int Color);
void GUI_Draw_ASCII_ARGB8888(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void GUI_Draw_String_ARGB8888(int x, int y, char *str, unsigned int Color1, unsigned int Color2, int fx, int fd);
void GUI_Draw_Picture_ARGB8888(int x, int y, int w, int h, char *pic);
void GUI_Draw_HZ1616_ARGB8888(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void GUI_Draw_HZ2424_ARGB8888(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void GUI_Draw_HZ3232_ARGB8888(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void GUI_Draw_HZ4040_ARGB8888(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void GUI_Draw_HZ4848_ARGB8888(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void GUI_Move_Draw_Picture_ARGB8888(int x, int y, int w, int h, char *pic);
int GUI_ReadTF_Draw_Picture_ARGB8888(int x, int y, char *path);
void GUI_Draw_Rectangle_ARGB8888_X(int x, int y, int w, int h, unsigned int Color);
void GUI_Draw_Picture24_ARGB8888(int x, int y, int w, int h, char *pic);
void GUI_Draw_Picture32_ARGB8888(int x, int y, int w, int h, char *pic);
void GUI_Draw_HZ_ARGB8888(int x, int y, int v, unsigned int Color1, unsigned int Color2, int fx, int fd, char *hzprt);
void GUI_Draw_Line_ARGB8888(int x0, int y0, int x1, int y1, unsigned int color, int fd);
void GUI_Draw_Rectangle_ARGB8888(int x, int y, int w, int h, unsigned int Color, int fill, int line_w);
void GUI_Draw_Circle_ARGB8888(int cx, int cy, int r, unsigned int color, int fill, int line_w);

#endif
