#include "sys_lcd.h"
#include "reg-debe.h"
#include "reg-tcon.h"
#include "sys_interrupt.h"
#include "sys_uart.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_io.h"
#include <stdlib.h>
//-----------------------
#include "sys_lcd_conf.h"
//-----------------------
#if (defined LCD_TYPE_TV_PAL_720_576) || (defined LCD_TYPE_TV_NTSC_720_480)
#include "sys_tve.h"
#endif

//LCD时序
__align(4) struct __lcd_timing lcd_timing=
{
/*模式 ----水平时序------,     -----垂直时序------,   -极性控制--,     串并行  CPU_MODE 	PCKL=(24MHz*N)/M/F  [5<F<96]*/
/*MODE WIDTH,HFP,HBP,HSPW,  HEIGHT,VFP,VBP,VSPW,  HA,VA,DA,CA,      s,  		  0, 		N, M, F,                    */
#ifdef	LCD_TYPE_RGB43_480_272
	 2,     480,  8, 43,  1,      272,  4, 12,  10,   0, 0, 1, 0,      1,  		  0, 		65, 4,40 //PCLK=9.75MHZ
#endif
#ifdef LCD_TYPE_RGB43_800_480
   2,     800, 40, 87,  1,      480, 13, 31,   1,   0, 0, 1, 0,      1,  		  0, 		65, 4,13 //PCLK=30MHZ
#endif
#ifdef LCD_TYPE_RGB70_1024_600
	 2,    1024,160,160,  1,      600,  2, 23,  12,   0, 0, 1, 0,      1,  		  0, 		65, 4, 8 //PCLK=48.75MHZ
#endif
//------------------------------VGA
#ifdef LCD_TYPE_Vga_640_480_60HZ
	 2,     640, 16, 48, 96,      480, 10, 33,   2,   0, 0, 1, 0,      1,  		  0, 		23, 2, 11//PCLK=25.09MHZ
#endif
#ifdef LCD_TYPE_Vga_640_480_75HZ
	 2,     640, 16,120, 64,      480,  1, 16,   3,   0, 0, 1, 0,      1,  		  0, 		63, 8, 6 //PCLK=31.5MHZ
#endif
#ifdef LCD_TYPE_Vga_1024_768_60HZ
	 2,    1024, 24,160,136,      768,  3, 29,   6,   0, 0, 1, 0,      1,  		  0, 		65, 4, 6 //PCLK=65MHZ
#endif
#ifdef LCD_TYPE_Vga_1280_720_60HZ
	 2,    1280,110,220, 40,      720,  5, 20,   5,   0, 0, 1, 0,      1,  		  0, 		99, 4, 8 //PCLK=74.25MHZ
#endif
#ifdef LCD_TYPE_Vga_1360_768_60HZ
	 2,    1360, 64,256,112,      768,  3, 18,   6,   0, 0, 1, 0,      1,  		  0, 		86, 4, 6 //PCLK=85.5MHZ
#endif
//------------------------------TV
#if (defined LCD_TYPE_TV_PAL_720_576)||(defined LCD_TYPE_TV_NTSC_720_480)
	 0
#endif
//------------------------------MCU 8位2串行65K色
#ifdef LCD_TYPE_MCU_320_240
	 0,      320,  1, 1,  1,      240,  1,  1,   1,    1, 1, 0, 1,      2,  	  7, 		65, 4, 12 //PCLK=MHZ
#endif

};

// 全局结构体
struct fb_f1c100s_pdata_t __lcd_pdat;
struct fb_f1c100s_pdata_t *lcd_pdat;

// RGB转YUV系数
static unsigned int csc_tab_rgb2yuv[4][12] =
	{
		{0x0204, 0x0107, 0x0064, 0x0100, 0x1ed6, 0x1f68, 0x01c1, 0x0800, 0x1e87, 0x01c1, 0x1fb7, 0x0800}, // bt601 rgb2yuv
		{0x0274, 0x00bb, 0x003f, 0x0100, 0x1ea5, 0x1f98, 0x01c1, 0x0800, 0x1e67, 0x01c1, 0x1fd7, 0x0800}, // bt709 rgb2yuv
		{0x0258, 0x0132, 0x0075, 0x0000, 0x1eac, 0x1f53, 0x0200, 0x0800, 0x1e53, 0x0200, 0x1fac, 0x0800}, // DISP_YCC rgb2yuv
		{0x0274, 0x00bb, 0x003f, 0x0100, 0x1ea5, 0x1f98, 0x01c1, 0x0800, 0x1e67, 0x01c1, 0x1fd7, 0x0800}  // xvYCC rgb2yuv
};
/*YUV使能输出*/
void YUV_OUT_Enable(char mode)
{
	int i;
	for (i = 0x950; i <= 0x97c; i += 4)
		write32(DEBE_Base_Address + i, csc_tab_rgb2yuv[mode][(i - 0x950) / 4] << 16);
	S_BIT(DEBE_Base_Address + 0x800, 5);
}
/*关TCON时钟*/
void Tcon_Clk_Close(void)
{
	C_BIT(CCU_BUS_CLK_GATING_REG1, 4);
	C_BIT(CCU_TCON_CLK_REG, 31);
	C_BIT(CCU_BUS_SOFT_RST_REG1, 4);
}
/*开TCON时钟*/
void Tcon_Clk_Open(void)
{
	S_BIT(CCU_BUS_CLK_GATING_REG1, 4);
	S_BIT(CCU_TCON_CLK_REG, 31);
	S_BIT(CCU_BUS_SOFT_RST_REG1, 4);
}
/*开Debe时钟*/
void Debe_Clk_Open(void)
{
	S_BIT(CCU_BUS_CLK_GATING_REG1, 12);
	S_BIT(CCU_BUS_SOFT_RST_REG1, 12);
}
/*关Debe时钟*/
void Debe_Clk_Close(void)
{
	C_BIT(CCU_BUS_CLK_GATING_REG1, 12);
	C_BIT(CCU_BUS_SOFT_RST_REG1, 12);
}
/*------------------------
设置层窗口大小
------------------------*/
void Set_LayerX_window(int Layer, int x, int y, int w, int h)
{
	struct f1c100s_debe_reg_t *debe = (struct f1c100s_debe_reg_t *)F1C100S_DEBE_BASE;
	switch (Layer)
	{
	case 0:
		write32((virtual_addr_t)&debe->layer0_pos, (y << 16) | (x << 0));
		write32((virtual_addr_t)&debe->layer0_size, ((h - 1) << 16) | ((w - 1) << 0));
		break;
	case 1:
		write32((virtual_addr_t)&debe->layer1_pos, (y << 16) | (x << 0));
		write32((virtual_addr_t)&debe->layer1_size, ((h - 1) << 16) | ((w - 1) << 0));
		break;
	case 2:
		write32((virtual_addr_t)&debe->layer2_pos, (y << 16) | (x << 0));
		write32((virtual_addr_t)&debe->layer2_size, ((h - 1) << 16) | ((w - 1) << 0));
		break;
	case 3:
		write32((virtual_addr_t)&debe->layer3_pos, (y << 16) | (x << 0));
		write32((virtual_addr_t)&debe->layer3_size, ((h - 1) << 16) | ((w - 1) << 0));
		break;
	}
}
/*------------------------
使能层视频通道
------------------------*/
void Enable_LayerX_Video(int Layer)
{
	struct f1c100s_debe_reg_t *debe = (struct f1c100s_debe_reg_t *)F1C100S_DEBE_BASE;
	switch (Layer)
	{
	case 0:
		S_BIT((virtual_addr_t)&debe->layer0_attr0_ctrl, 1);
		break;
	case 1:
		S_BIT((virtual_addr_t)&debe->layer1_attr0_ctrl, 1);
		break;
	case 2:
		S_BIT((virtual_addr_t)&debe->layer2_attr0_ctrl, 1);
		break;
	case 3:
		S_BIT((virtual_addr_t)&debe->layer3_attr0_ctrl, 1);
		break;
	}
}
/*------------------------
失能层视频通道
------------------------*/
void Disable_LayerX_Video(int Layer)
{
	struct f1c100s_debe_reg_t *debe = (struct f1c100s_debe_reg_t *)F1C100S_DEBE_BASE;
	switch (Layer)
	{
	case 0:
		C_BIT((virtual_addr_t)&debe->layer0_attr0_ctrl, 1);
		break;
	case 1:
		C_BIT((virtual_addr_t)&debe->layer1_attr0_ctrl, 1);
		break;
	case 2:
		C_BIT((virtual_addr_t)&debe->layer2_attr0_ctrl, 1);
		break;
	case 3:
		C_BIT((virtual_addr_t)&debe->layer3_attr0_ctrl, 1);
		break;
	}
}
/*------------------------
设置数据格式
[forma RGB565=0x5,stride=4][forma ARGB8888=0xA,stride=5]
------------------------*/
void Set_Layer_format(int Layer, int format, int stride)
{
	struct f1c100s_debe_reg_t *debe = (struct f1c100s_debe_reg_t *)F1C100S_DEBE_BASE;
	switch (Layer)
	{
	case 0:
		write32((virtual_addr_t)&debe->layer0_attr1_ctrl, format << 8);
		write32((virtual_addr_t)&debe->layer0_stride, ((lcd_timing.width) << stride));
		break;
	case 1:
		write32((virtual_addr_t)&debe->layer1_attr1_ctrl, format << 8);
		write32((virtual_addr_t)&debe->layer1_stride, ((lcd_timing.width) << stride));
		break;
	case 2:
		write32((virtual_addr_t)&debe->layer2_attr1_ctrl, format << 8);
		write32((virtual_addr_t)&debe->layer2_stride, ((lcd_timing.width) << stride));
		break;
	case 3:
		write32((virtual_addr_t)&debe->layer3_attr1_ctrl, format << 8);
		write32((virtual_addr_t)&debe->layer3_stride, ((lcd_timing.width) << stride));
		break;
	}
}

/*------------------------
设置层0视频窗口位置与大小
------------------------*/
void set_video_window(int x, int y, int w, int h)
{
	Set_LayerX_window(0, x, y, w, h);
}
/*------------------------
使能层0视频通道
------------------------*/
void Enable_Layer_Video(void)
{
	Enable_LayerX_Video(0);
}
/*------------------------
失能层0视频通道
------------------------*/
void Disable_Layer_Video(void)
{
	Disable_LayerX_Video(0);
}

/*TCON初始化*/
void Tcon_Init(struct fb_f1c100s_pdata_t *pdat)
{
	struct f1c100s_tcon_reg_t *tcon = ((struct f1c100s_tcon_reg_t *)pdat->virttcon);
	int val;
	int bp, total;
	/*设置VIDEO PLL时钟*/
	C_BIT(CCU_Base_Address + 0x010, 31);
	write32(CCU_Base_Address + 0x010, ((lcd_timing.n - 1) << 8) | ((lcd_timing.m - 1) << 0) | (3 << 24));
	S_BIT(CCU_Base_Address + 0x010, 31);
	delay_ms(1);
	/*设置TCON时钟与复位*/
	S_BIT(CCU_BUS_CLK_GATING_REG1, 4);
	S_BIT(CCU_TCON_CLK_REG, 31);
	S_BIT(CCU_BUS_SOFT_RST_REG1, 4);
	delay_ms(1);
	/*TCON配置*/
	C_BIT(TCON_Base_Address + 0x40, 0);
	val = (lcd_timing.v_front_porch + lcd_timing.v_back_porch + lcd_timing.v_sync_len);
	write32(TCON_Base_Address + 0x40, ((u64_t)0x1 << 31) | (val & 0x1f) << 4);
	val = lcd_timing.f; // 5< DCLKDIV <96 时钟除数
	write32(TCON_Base_Address + 0x44, ((u64_t)0xf << 28) | (val << 0));
	/*设置宽高*/
	write32((virtual_addr_t)&tcon->tcon0_timing_active, ((lcd_timing.width - 1) << 16) | ((lcd_timing.height - 1) << 0));
	/*设置HT+HBP*/
	bp = lcd_timing.h_sync_len + lcd_timing.h_back_porch;
	total = (lcd_timing.width * lcd_timing.serial) + lcd_timing.h_front_porch + bp;
	write32((virtual_addr_t)&tcon->tcon0_timing_h, ((total - 1) << 16) | ((bp - 1) << 0));
	/*设置VT+VBP*/
	bp = lcd_timing.v_sync_len + lcd_timing.v_back_porch;
	total = lcd_timing.height + lcd_timing.v_front_porch + bp;
	write32((virtual_addr_t)&tcon->tcon0_timing_v, ((total * 2) << 16) | ((bp - 1) << 0));
	/*设置时钟宽度*/
	write32((virtual_addr_t)&tcon->tcon0_timing_sync, ((lcd_timing.h_sync_len - 1) << 16) | ((lcd_timing.v_sync_len - 1) << 0));
/*设置RB交换-方便布线*/
#if 0
  S_BIT(TCON_Base_Address+0x40,23);
#endif
	/*设置模式*/
	if (lcd_timing.mode > 0) // rgb
	{
		write32((virtual_addr_t)&tcon->tcon0_hv_intf, 0);
		write32((virtual_addr_t)&tcon->tcon0_cpu_intf, 0);
	}
	else // cpu
	{
		// 设置为8080模式
		write32(TCON_Base_Address + 0x40, read32(TCON_Base_Address + 0x40) | (1) << 24);
		// 设置输入源
		write32(TCON_Base_Address + 0x40, read32(TCON_Base_Address + 0x40) | (0) << 0); //[3-白色数据][2-DMA][0-DE]
		// CPU模式
		write32(TCON_Base_Address + 0x60, (u64_t)(lcd_timing.cpu_mode) << 29 | (u64_t)(1) << 28);
	}
	// FRM
	if (pdat->bits_per_pixel == 18 || pdat->bits_per_pixel == 16)
	{
		write32((virtual_addr_t)&tcon->tcon0_frm_seed[0], 0x11111111);
		write32((virtual_addr_t)&tcon->tcon0_frm_seed[1], 0x11111111);
		write32((virtual_addr_t)&tcon->tcon0_frm_seed[2], 0x11111111);
		write32((virtual_addr_t)&tcon->tcon0_frm_seed[3], 0x11111111);
		write32((virtual_addr_t)&tcon->tcon0_frm_seed[4], 0x11111111);
		write32((virtual_addr_t)&tcon->tcon0_frm_seed[5], 0x11111111);
		write32((virtual_addr_t)&tcon->tcon0_frm_table[0], 0x01010000);
		write32((virtual_addr_t)&tcon->tcon0_frm_table[1], 0x15151111);
		write32((virtual_addr_t)&tcon->tcon0_frm_table[2], 0x57575555);
		write32((virtual_addr_t)&tcon->tcon0_frm_table[3], 0x7f7f7777);
		write32((virtual_addr_t)&tcon->tcon0_frm_ctrl, (pdat->bits_per_pixel == 18) ? (((u64_t)1 << 31) | (0 << 4)) : (((u64_t)1 << 31) | (5 << 4)));
	}
	// 极性控制
	val = (1 << 28);
	if (!lcd_timing.h_sync_active)
		val |= (1 << 25);
	if (!lcd_timing.v_sync_active)
		val |= (1 << 24);
	if (!lcd_timing.den_active)
		val |= (1 << 27);
	if (!lcd_timing.clk_active)
		val |= (1 << 26);
	write32((virtual_addr_t)&tcon->tcon0_io_polarity, val);
	// 触发控制关
	write32((virtual_addr_t)&tcon->tcon0_io_tristate, 0);
#if 0 // 中断配置参考数据手册183-184页
	//中断配置
	IRQ_Init(IRQ_LEVEL_1,IRQ_TCON,TCON_ISR ,3);
#if 1 // 消隐中断
	S_BIT(TCON_Base_Address+0x04,31);//TCON0 vertical blanking interrupt Enable
#endif
#if 0 // 线中断
	write32(TCON_Base_Address+0x08,(pdat->height)<<16);//set line
	S_BIT(TCON_Base_Address+0x04,29);//TCON0 line trigger interrupt enable
#endif
  sysSetLocalInterrupt(ENABLE_IRQ);//开IRQ中断
#endif
}
/*debe配置*/
void Debe_Init(struct fb_f1c100s_pdata_t *pdat)
{
	struct f1c100s_debe_reg_t *debe = (struct f1c100s_debe_reg_t *)pdat->virtdebe;
	// 时钟与复位
	S_BIT(CCU_BUS_CLK_GATING_REG1, 12);
	S_BIT(CCU_BUS_SOFT_RST_REG1, 12);
	delay_ms(1);
	// 使能DEBE
	S_BIT((virtual_addr_t)&debe->mode, 0);
	write32((virtual_addr_t)&debe->disp_size, (((pdat->height) - 1) << 16) | (((pdat->width) - 1) << 0));
	// 设置层0参数
	write32((virtual_addr_t)&debe->layer0_size, (((pdat->height) - 1) << 16) | (((pdat->width) - 1) << 0));
	write32((virtual_addr_t)&debe->layer0_stride, ((pdat->width) << 4)); //[RGB565=4][ARGB8888=5]
	write32((virtual_addr_t)&debe->layer0_addr_low32b, (u32)(pdat->vram[0]) << 3);
	write32((virtual_addr_t)&debe->layer0_addr_high4b, (u32)(pdat->vram[0]) >> 29);
	write32((virtual_addr_t)&debe->layer0_attr1_ctrl, 0x5 << 8); //[RGB565=0x5][ARGB8888=0xA]
	S_BIT((virtual_addr_t)&debe->mode, 8);						 // 层0使能
	if ((pdat->vram[1] != 0) && (pdat->vram[0] != pdat->vram[1]))
	{
		// 设置层1参数
		write32((virtual_addr_t)&debe->disp_size, (((pdat->height) - 1) << 16) | (((pdat->width) - 1) << 0));
		write32((virtual_addr_t)&debe->layer1_size, (((pdat->height) - 1) << 16) | (((pdat->width) - 1) << 0));
		write32((virtual_addr_t)&debe->layer1_stride, ((pdat->width) << 5)); //[RGB565=4][ARGB8888=5]
		write32((virtual_addr_t)&debe->layer1_addr_low32b, (u32)(pdat->vram[1]) << 3);
		write32((virtual_addr_t)&debe->layer1_addr_high4b, (u32)(pdat->vram[1]) >> 29);
		write32((virtual_addr_t)&debe->layer1_attr1_ctrl, 0x0A << 8); //[RGB565=0x5][ARGB8888=0xA]
		S_BIT((virtual_addr_t)&debe->layer1_attr0_ctrl, 10);		  // 优先级
		S_BIT((virtual_addr_t)&debe->layer1_attr0_ctrl, 15);		  // 1: select Pipe 1 需要透明叠加时需要输入不同管道
		S_BIT((virtual_addr_t)&debe->mode, 9);						  // 层1使能
	}
	// 加载
	S_BIT((virtual_addr_t)&debe->reg_ctrl, 0);
	// DEBE 开始
	S_BIT((virtual_addr_t)&debe->mode, 1);
}
/*debe设置地址*/
void f1c100s_debe_set_address(struct fb_f1c100s_pdata_t *pdat, void *vram)
{
	struct f1c100s_debe_reg_t *debe = (struct f1c100s_debe_reg_t *)pdat->virtdebe;

	write32((virtual_addr_t)&debe->layer0_addr_low32b, (u32_t)vram << 3);
	write32((virtual_addr_t)&debe->layer0_addr_high4b, (u32_t)vram >> 29);
}
/*tcon使能*/
void f1c100s_tcon_enable(struct fb_f1c100s_pdata_t *pdat)
{
	struct f1c100s_tcon_reg_t *tcon = (struct f1c100s_tcon_reg_t *)pdat->virttcon;
	u32_t val;

	val = read32((virtual_addr_t)&tcon->ctrl);
	val |= ((u64_t)1 << 31);
	write32((virtual_addr_t)&tcon->ctrl, val);
}
/*tcon失能*/
void f1c100s_tcon_disable(struct fb_f1c100s_pdata_t *pdat)
{
	struct f1c100s_tcon_reg_t *tcon = (struct f1c100s_tcon_reg_t *)pdat->virttcon;
	u32_t val;

	write32((virtual_addr_t)&tcon->ctrl, 0);
	write32((virtual_addr_t)&tcon->int0, 0);

	val = read32((virtual_addr_t)&tcon->tcon0_dclk);
	val &= ~((u64_t)0xf << 28);
	write32((virtual_addr_t)&tcon->tcon0_dclk, val);

	write32((virtual_addr_t)&tcon->tcon0_io_tristate, 0xffffffff);
	write32((virtual_addr_t)&tcon->tcon1_io_tristate, 0xffffffff);
}
/*LCD配置*/
void F1C100S_LCD_Config(int width, int height, unsigned int *buff1, unsigned int *buff2)
{
	int i;
	lcd_pdat = &__lcd_pdat;
	// 设置地址
	lcd_pdat->virtdebe = F1C100S_DEBE_BASE;
	lcd_pdat->virttcon = F1C100S_TCON_BASE;
	// LCD宽高
	lcd_pdat->width = width;
	lcd_pdat->height = height;
	// 像素位宽
	if (lcd_timing.mode > 1)
		lcd_pdat->bits_per_pixel = 18;
	else
		lcd_pdat->bits_per_pixel = 0; /*MCU屏设置为0*/

	// 设置缓存
	lcd_pdat->vram[0] = buff1;
	lcd_pdat->vram[1] = buff2;
	// 清寄存器
	for (i = 0x0800; i < 0x1000; i += 4)
		write32(F1C100S_DEBE_BASE + i, 0);
	// debe+tcon配置
	f1c100s_tcon_disable(lcd_pdat);
	Debe_Init(lcd_pdat);
	Tcon_Init(lcd_pdat);
	// 使能TV，TV与LCD输出只能2选1
#if (defined LCD_TYPE_TV_PAL_720_576) || (defined LCD_TYPE_TV_NTSC_720_480)
	TVE_Init();
	YUV_OUT_Enable(0);
#endif
	f1c100s_tcon_enable(lcd_pdat);
}
/*LCD IO初始化*/
void LCD_IO_Init(void)
{
	int i = 0;
	//
	if (lcd_timing.mode > 0) // RGB
	{
		if (lcd_timing.mode == 1) // RGB565
		{
			for (i = 18; i <= 21; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
			for (i = 1; i <= 8; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
			for (i = 10; i <= 17; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
		}
		else if (lcd_timing.mode == 2) // RGB666
		{
			for (i = 18; i <= 21; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
			for (i = 0; i <= 8; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
			for (i = 9; i <= 17; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
		}
		else if (lcd_timing.mode == 3) // RGB888
		{
			for (i = 18; i <= 21; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
			for (i = 0; i <= 8; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
			for (i = 9; i <= 17; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
			for (i = 0; i <= 5; i++)
				GPIO_Congif(GPIOE, GPIO_Pin_0 + i, GPIO_Mode_011, GPIO_PuPd_NOPULL);
		}
		else if (lcd_timing.mode == 4) // RGB8位串行
		{
			for (i = 18; i <= 21; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
			for (i = 1; i <= 8; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
		}
		else if (lcd_timing.mode == 5) // CCIR656
		{
			GPIO_Congif(GPIOD, GPIO_Pin_18, GPIO_Mode_010, GPIO_PuPd_NOPULL); // 只需要一个时钟线
			for (i = 1; i <= 8; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
		}
	}
	else // MCU
	{
		for (i = 18; i <= 21; i++)
			GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
		if ((lcd_timing.cpu_mode == 6) || (lcd_timing.cpu_mode == 7)) // MCU 8位
		{
			for (i = 1; i <= 8; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
		}
		else if ((lcd_timing.cpu_mode == 1) || (lcd_timing.cpu_mode == 2) || (lcd_timing.cpu_mode == 3) || (lcd_timing.cpu_mode == 4)) // MCU 16位
		{
			for (i = 1; i <= 8; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
			for (i = 10; i <= 17; i++)
				GPIO_Congif(GPIOD, GPIO_Pin_0 + i, GPIO_Mode_010, GPIO_PuPd_NOPULL);
		}
	}
}
/*LCD初始化*/
void Sys_LCD_Init(int width, int height, unsigned int *buff1, unsigned int *buff2)
{
	// 多次初始化时要先关时钟
	Tcon_Clk_Close();
	Debe_Clk_Close();
	LCD_IO_Init();									 // IO初始化
	F1C100S_LCD_Config(width, height, buff1, buff2); // TCON DEBE 初始化
}
/*TCON中断*/
void TCON_ISR(void)
{
	// 这里TCON_INT_REG0需要写0清除中断标志-参考数据手册183页
	C_BIT(TCON_Base_Address + 0x04, 15); // 清除TCON0消隐中断
}
