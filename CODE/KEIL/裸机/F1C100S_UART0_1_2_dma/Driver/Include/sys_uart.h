#ifndef __SYS_UART_H__
#define __SYS_UART_H__

#include "sys_clock.h"
#include "sys_io.h"

#define UART0_ADDR 0x01c25000
#define UART1_ADDR 0x01C25400
#define UART2_ADDR 0x01c25800

enum eUart
{
	UART0 = 0,
	UART1,
	UART2
};

#define get_uart_clk_state(UARTx) (read32(0x01c20068) & (1 << (20 + UARTx))) /*返回串口时钟是否打开*/
#define sysprintf_null printf_null
#define Sys_Uart0_Init(x) Sys_Uart_Init(UART0, get_cpu_frequency(), x, 0)
#define sys_Uart0_Init(a, x) Sys_Uart_Init(UART0, a, x, 0)

void Sys_Uart_Init(enum eUart uart, unsigned int cpu_frequency, unsigned int Baudrate, int Interrupt);
void Sys_Uart_Put(enum eUart uart, char c);
void sysprintf(char *pcStr, ...);
void Sys_SET_UART_DBG(enum eUart uart);
void printf_null(char *pcStr, ...);

void sysprintf_r(char *pcStr, ...);
void sysprintf_rn(char *pcStr, ...);
void Sys_Uart_Putc(char c);

#endif
