#include "sys_uart.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_timer.h"
#include <string.h>
#include <stdio.h>
#include <sys_mmu.h>

/*---------------------------------------------------
---- main
---- main
----------------------------------------------------*/
int main(void)
{
	Sys_Clock_Init(408000000);
	sys_mmu_init();
	// Sys_Uart_Init(UART0, 408000000, 115200, 0); // 打印

	Sys_Uart_Init(UART0, 408000000, 115200, 1); // 收发中断测试
	sysSetLocalInterrupt(ENABLE_IRQ);			// 开总中断

	sysprintf("\r\n\r\n\r\n");
	sysprintf("Sys_Start...\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);

	sysprintf("UART_TEST...\r\n");

	while (1)
	{
	}
}
