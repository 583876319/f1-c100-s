/**
 * @file sys_delay.h
 * @brief
 * @details
 * @author Mars
 * @date 2023-10-22
 * @version V1.0.0
 * @copyright
 */
#ifndef _SYS_DELAY_H_
#define _SYS_DELAY_H_

void delay_us(int us);
void delay_ms(int ms);

#endif // SYS_DELAY_H
