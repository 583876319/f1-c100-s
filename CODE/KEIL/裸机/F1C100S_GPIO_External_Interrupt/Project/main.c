#include "sys_uart.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_timer.h"
#include <string.h>
#include <stdio.h>
#include <sys_mmu.h>

// IOE中断
void PIOE_ISR(void)
{
	static int f = 0, s = 0;

	int val = GPIO_External_Inerrupt_Status(GPIOEI);
	if ((val & (0x1 << GPIO_Pin_3)) && (s == 1))
	{
		if (f == 0)
		{
			GPIO_External_Inerrupt_Config(GPIOEI, GPIO_Pin_3, High_Level); // 高电平触发
			f = 1;
			sysprintf("GPIOE_3 DOWN \r\n");
		}
		else
		{
			GPIO_External_Inerrupt_Config(GPIOEI, GPIO_Pin_3, Low_Level); // 低电平触发
			f = 0;
			sysprintf("GPIOE_3 UP \r\n");
		}
	}
	s = 1;
}

/*---------------------------------------------------
---- main
---- main
----------------------------------------------------*/
int main(void)
{
	Sys_Clock_Init(408000000);
	sys_mmu_init();
	Sys_Uart0_Init(115200);

	sysprintf("\r\n\r\n\r\n");
	sysprintf("Sys_Start...\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);

	sysprintf("GPIO_External_Inerrupt Test...\r\n");
	// gpio配置
	GPIO_Congif(GPIOE, GPIO_Pin_3, GPIO_Mode_110, GPIO_PuPd_UP);
	GPIO_External_Inerrupt_Config(GPIOEI, GPIO_Pin_3, Low_Level); // 低电平触发
	GPIO_External_Inerrupt_Enable(GPIOEI, GPIO_Pin_3);			  // 中断使能
	// 全局中断
	IRQ_Init(IRQ_LEVEL_1, IRQ_PIOE, PIOE_ISR, 3);
	sysSetLocalInterrupt(ENABLE_IRQ); // 开IRQ中断

	while (1)
	{
		// sysprintf("RUN...\r\n");
		delay_ms(1000);
	}
	return 0;
}
