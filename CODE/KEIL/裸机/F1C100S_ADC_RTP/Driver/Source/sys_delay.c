#include "sys_delay.h"
#include "sys_timer.h"

// 使用AV定时器来做延时处理
#define AVS_TIME_x AVS_TIME_1
/*
延时--毫秒
*/
void delay_ms(int ms)
{
  AVS_Time_Init(AVS_TIME_x, 1000); // 1000us计时一次
  AVS_Time_Start(AVS_TIME_x);
  while (1)
  {
    if (AVS_Time_Read(AVS_TIME_x) >= ms)
      break;
  }
}
/*
延时--微秒（程序初始化可能会导致1-5us误差，只能做大概延时）
*/
void delay_us(int us)
{
  AVS_Time_Init(AVS_TIME_x, 1); // 1us计时一次
  AVS_Time_Start(AVS_TIME_x);
  while (1)
  {
    if (AVS_Time_Read(AVS_TIME_x) >= us)
      break;
  }
}
