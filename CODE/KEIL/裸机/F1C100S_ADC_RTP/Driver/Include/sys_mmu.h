/**
 * @file sys_mmu.h
 * @brief
 * @details
 * @author Mars
 * @date 2023-10-22
 * @version V1.0.0
 * @copyright
 */
#ifndef _SYS_MMU_H_
#define _SYS_MMU_H_

void sys_mmu_init(void);

#endif // SYS_MMU_H
