﻿/**
 * @file sys_TPAdc.h
 * @brief
 * @details
 * @author Mars
 * @date 2023-10-22
 * @version V1.0.0
 * @copyright
 */
#ifndef _SYS_TPADC_H_
#define _SYS_TPADC_H_

void TP_MODE_Demo(void);
void ADC_MODE_Demo(void);
void TP_MODE_Demo(void);

#endif // SYS_TPADC_H
