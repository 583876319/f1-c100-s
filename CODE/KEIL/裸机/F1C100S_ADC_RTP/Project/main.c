#include "sys_uart.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_timer.h"
#include "sys_mmu.h"
#include "sys_TPAdc.h"
#include "sys_keyAdc.h"

/*---------------------------------------------------
---- main
---- main
----------------------------------------------------*/
int main(void)
{
	Sys_Clock_Init(408000000);
	sys_mmu_init();
	Sys_Uart0_Init(115200);

	sysprintf("\r\n\r\n\r\n");
	sysprintf("Sys_Start...\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);

	Timer1_Init();
	/*-------选择不同功能测试-------*/
	// KeyADC_Demo(); // 测试按键KEY-ADC
	TP_MODE_Demo(); // 测试电阻触摸屏(RTP)
	// ADC_MODE_Demo();//测试ADC(RTP功能设置成通用ADC使用)

	while (1)
	{
	}
}
