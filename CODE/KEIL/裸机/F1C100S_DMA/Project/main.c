#include "sys_uart.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_timer.h"
#include <string.h>
#include <stdio.h>
#include <sys_mmu.h>
#include "sys_dma.h"

/*
NDMA 测试
复制数据
*/
__align(4) int sbuff[250000 / 4];
__align(4) int dbuff[250000 / 4];
int Nlen = 250000 / 4;
void NDMA_Demo(void)
{
	int i, time, l = 0;
	DMA dma;

	sysprintf("NDMA_Demo...\r\n");
	//-----------
	DMA_Init();
	for (i = 0; i < Nlen; i++)
	{
		sbuff[i] = i & 0xff;
	}
	for (i = 0; i < Nlen; i++)
	{
		dbuff[i] = 0;
	}
	//---------
	dma.Type = NDMA;				  // 类型
	dma.Ch = 0;						  // 通道
	dma.Byte_Counter = sizeof(sbuff); // Byte计数
	dma.Continuous_Mode_Enable = 0;	  // 连续模式
	dma.Read_Byte_Counter_Enable = 0; // 读计数值使能
	//---------
	dma.Source_Address = (unsigned int)sbuff;		   // 源地址
	dma.Source_Address_Type = DMA_ADDRESS_TYEP_LINEER; // 源地址类型
	dma.Source_DRQ_Type = NDMAS_DRQ_Type_SDRAM_Memory; // 源类型
	dma.Source_Data_Width = DMA_DATA_WIDTH_32;		   // 源数据宽度
	dma.Source_Burst_Length = DMA_BURST_LENGTH_4;	   // BURST
	//-----------
	dma.Destination_Address = (unsigned int)dbuff;			// 目标地址
	dma.Destination_Address_Type = DMA_ADDRESS_TYEP_LINEER; // 目标地址类型
	dma.Destination_DRQ_Type = NDMAD_DRQ_Type_SDRAM_Memory; // 目标类型
	dma.Destination_Data_Width = DMA_DATA_WIDTH_32;			// 目标数据宽度
	dma.Destination_Burst_Length = DMA_BURST_LENGTH_4;		// BURST
	//-----------
	sysprintf("复制数据大小: %d KB\r\n", dma.Byte_Counter / 1024);
	time = Read_time_ms();
	DMA_Config(&dma);
	DMA_Enable(&dma);
	while (1)
	{
		if (DMA_Get_Full_TIP(&dma))
		{
			// sysInvalidCache();//cache
			sysprintf("复制完成  用时：%d  ms\r\n", Read_time_ms() - time);
			sysprintf("复制完成  开始验证 \r\n");

			for (i = 0; i < Nlen; i += Nlen / 10)
			{
				sysprintf("sbuff[i]=%d \r\n", sbuff[i]);
			}
			for (i = 0; i < Nlen; i += Nlen / 10)
			{
				sysprintf("dbuff[i]=%d \r\n", dbuff[i]);
			}
			for (i = 0; i < Nlen; i++)
			{
				if (sbuff[i] == dbuff[i])
				{
					l++;
				}
			}
			sysprintf("l=%d \r\n", l);
			if (l == Nlen)
			{
				sysprintf("验证成功\r\n");
			}
			else
			{
				sysprintf("验证失败\r\n");
			}
			break;
		}
	}
}
/*
DDMA 测试
复制数据
*/
__align(4) int dsbuff[1024 * 1024 * 2];
__align(4) int ddbuff[1024 * 1024 * 2];
int len = 1024 * 1024 * 2;
void DDMA_Demo(void)
{
	int i, time, l = 0;
	DMA dma;

	sysprintf("DDMA_Demo...\r\n");
	//-----------
	DMA_Init();
	for (i = 0; i < len; i++)
	{
		dsbuff[i] = i & 0xff;
	}
	for (i = 0; i < len; i++)
	{
		ddbuff[i] = 0;
	}

	//---------
	dma.Type = DDMA;				   // 类型
	dma.Ch = 0;						   // 通道
	dma.Byte_Counter = sizeof(dsbuff); // Byte计数
	dma.Continuous_Mode_Enable = 0;	   // 连续模式
	dma.Read_Byte_Counter_Enable = 0;  // 读计数值使能
	//---------
	dma.Source_Address = (unsigned int)dsbuff;		   // 源地址
	dma.Source_Address_Type = DMA_ADDRESS_TYEP_LINEER; // 源地址类型
	dma.Source_DRQ_Type = DDMAS_DRQ_Type_SDRAM_Memory; // 源类型
	dma.Source_Data_Width = DMA_DATA_WIDTH_32;		   // 源数据宽度
	dma.Source_Burst_Length = DMA_BURST_LENGTH_4;	   // BURST
	//-----------
	dma.Destination_Address = (unsigned int)ddbuff;			// 目标地址
	dma.Destination_Address_Type = DMA_ADDRESS_TYEP_LINEER; // 目标地址类型
	dma.Destination_DRQ_Type = DDMAD_DRQ_Type_SDRAM_Memory; // 目标类型
	dma.Destination_Data_Width = DMA_DATA_WIDTH_32;			// 目标数据宽度
	dma.Destination_Burst_Length = DMA_BURST_LENGTH_4;		// BURST
	//-----------
	sysprintf("复制数据大小: %d KB\r\n", dma.Byte_Counter / 1024);
	time = Read_time_ms();
	DMA_Config(&dma);
	DMA_Enable(&dma);

	while (1)
	{
		if (DMA_Get_Full_TIP(&dma))
		{
			// sysInvalidCache();//cache
			sysprintf("复制完成  用时：%d  ms\r\n", Read_time_ms() - time);

			sysprintf("复制完成  开始验证 \r\n");

			for (i = 0; i < len; i += len / 10)
			{
				sysprintf("sbuff[i]=%d \r\n", dsbuff[i]);
			}
			for (i = 0; i < len; i += len / 10)
			{
				sysprintf("dbuff[i]=%d \r\n", ddbuff[i]);
			}
			for (i = 0; i < len; i++)
			{
				if (dsbuff[i] == ddbuff[i])
				{
					l++;
				}
			}
			sysprintf("l=%d \r\n", l);
			if (l == len)
			{
				sysprintf("验证成功\r\n");
			}
			else
			{
				sysprintf("验证失败\r\n");
			}
			break;
		}
	}
}

/*---------------------------------------------------
---- main
---- main
----------------------------------------------------*/
int main(void)
{
	Sys_Clock_Init(408);
	sys_mmu_init();
	Sys_Uart0_Init(115200);

	sysprintf("\r\n\r\n\r\n");
	sysprintf("Sys_Start...\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);
	Sys_Timer1_Init();

	DDMA_Demo();
	NDMA_Demo();

	while (1)
	{
	}
	return 0;
}
