#include "sys_uart.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_wdog.h"
#include "sys_timer.h"
#include <string.h>
#include <stdio.h>
#include <sys_mmu.h>
/*---------------------------------------------------
修改说明
	2020-08-18
		1.增加关闭WDOG函数。
		2.修改初始化函数初始化定时值前先清除定时值位,避免重新初始化定时值不准。
----------------------------------------------------*/

/*---------------------------------------------------
---- main
---- main
----------------------------------------------------*/
int main(void)
{
	Sys_Clock_Init(408000000);
	sys_mmu_init();
	sys_Uart0_Init(408000000, 115200);
	sysprintf("\r\n\r\n\r\n");
	sysprintf("Sys_Start...\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);
	Timer1_Init();

	Wdog_Init(TimeOut_1s, WDOG_WholeSystemMode, 0); // 大于1秒没有更新看门狗将复位
	Wdog_enable();									// 看门狗使能

	while (1)
	{
		Wdog_Restart(); // 喂狗
		sysprintf("Wdog...\r\n");
		delay_ms(1800); // 这里设置为700ms程序正常运行，设置为800ms程序会复位
	}

	return 0;
}
