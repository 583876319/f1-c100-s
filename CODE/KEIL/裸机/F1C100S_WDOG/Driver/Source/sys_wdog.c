#include "sys_wdog.h"
#include "sys_interrupt.h"
#include "sys_uart.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_io.h"

#define F1C100S_TIMER_BASE (0x01C20C00)
#define WDOG_IRQ_EN        F1C100S_TIMER_BASE + (0xA0)
#define WDOG_IRQ_STA       F1C100S_TIMER_BASE + (0xA4)
#define WDOG_CRTL_REG      F1C100S_TIMER_BASE + (0xB0)
#define WDOG_CFG_REG       F1C100S_TIMER_BASE + (0xB4)
#define WDOG_MODE_REG      F1C100S_TIMER_BASE + (0xB8)

/*
看门狗初始化
IRQ_EN=中断使能
Mode 1=全模式 2=只中断
TimeOutUs=超时时间 us
*/
void Wdog_Init(u32_t TimeOutUs, u8 Mode, u8 IRQ_EN)
{
	// 设置模式
	write32(WDOG_CFG_REG, read32(WDOG_CFG_REG) | ((Mode) << 0));
	// 设置超时时间
	write32(WDOG_MODE_REG, (read32(WDOG_MODE_REG) & (~(0xf << 4))) | (TimeOutUs << 4));
	// 使能中断
	if (IRQ_EN == 1)
	{
		write32(WDOG_IRQ_EN, read32(WDOG_IRQ_EN) | (1) << 0);
	}
}
/*
WDOG开
*/
void Wdog_enable(void)
{
	write32(WDOG_MODE_REG, read32(WDOG_MODE_REG) | ((1) << 0));
}
/*
WDOG关
*/
void Wdog_disable(void)
{
	write32(WDOG_MODE_REG, read32(WDOG_MODE_REG) & (~((1) << 0)));
}
/*
喂狗
*/
void Wdog_Restart(void)
{
	write32(WDOG_CRTL_REG, read32(WDOG_CRTL_REG) | ((0xA57) << 1) | ((1) << 0));
}
/*
Wdog中断
*/
void Wdog_ISR(void)
{
	// 清中断
	write32(WDOG_IRQ_STA, read32(WDOG_IRQ_STA) | (1 << 0));
}
/*
Wdog测试-【有一定误差为正常】
*/
void Wdog_Demo(void)
{
	Wdog_Init(TimeOut_1s, WDOG_WholeSystemMode, 0); // 大于1秒没有更新看门狗将复位
	Wdog_enable();									// 看门狗使能

	while (1)
	{
		Wdog_Restart(); // 喂狗
		sysprintf("Wdog...\r\n");
		delay_ms(800); // 这里设置为700ms程序正常运行，设置为800ms程序会复位
	}
}
