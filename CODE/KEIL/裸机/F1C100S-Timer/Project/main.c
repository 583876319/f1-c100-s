#include "sys_uart.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_timer.h"
#include <string.h>
#include <stdio.h>
#include "sys_io.h"
#include "sys_mmu.h"

/*---------------------------------------------------
---- main
---- main
----------------------------------------------------*/
int main(void)
{
	int i = 0, h = 0, m = 0, s = 0, n = 0;
	char str[20];

	Sys_Clock_Init(408000000);
	sys_mmu_init();
	Sys_Uart0_Init(115200);

	sysprintf("\r\n\r\n\r\n");
	sysprintf("Sys_Start...\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);

	// 串口打印中断计数值
	Sys_TimerX_Init(TIMER1, 1000, TIMER1_ISR, 1);
	while (1)
	{
		sysprintf("TIMER %d \r\n", Read_time_ms());
		delay_ms(1000);
	}
}
