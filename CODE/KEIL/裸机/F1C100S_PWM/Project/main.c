#include "sys_uart.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_timer.h"
#include <string.h>
#include <stdio.h>
#include <sys_mmu.h>
#include "sys_pwm.h"

/*---------------------------------------------------
---- main
---- main
----------------------------------------------------*/
int main(void)
{
	Sys_Clock_Init(408000000);
	sys_mmu_init();
	sys_Uart0_Init(408000000, 115200);

	sysprintf("\r\n\r\n\r\n");
	sysprintf("Sys_Start...\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);

	int i;
	int Ch = 1;
	sysprintf("PWM_Demo...\r\n");
	PWM_Init(Ch, 1000, 50, 1);
	delay_ms(2000);

	GPIO_Congif(GPIOE, GPIO_Pin_5, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);
	GPIO_Congif(GPIOE, GPIO_Pin_11, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);

	GPIO_SET(GPIOE, GPIO_Pin_5);
	GPIO_SET(GPIOE, GPIO_Pin_11);

	while (1)
	{
		for (i = 0; i < 100; i++)
		{
			delay_ms(10);
			PWM_Init(Ch, 100000, i, 1);
		}
		for (i = 100; i > 0; i--)
		{
			delay_ms(10);
			PWM_Init(Ch, 100000, i, 1);
		}
	}

	return 0;
}
