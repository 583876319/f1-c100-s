/**
 * @file sys_pwm.h
 * @brief
 * @details
 * @author Mars
 * @date 2023-10-22
 * @version V1.0.0
 * @copyright
 */
#ifndef _SYS_PWM_H_
#define _SYS_PWM_H_

void PWM_Demo(void);
void PWM_Init(int channel, int frequency_hz, float pwm, int polarity);

#endif // SYS_PWM_H
