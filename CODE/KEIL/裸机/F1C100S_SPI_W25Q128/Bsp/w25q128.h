/**
 * @file w25q128.h
 * @brief
 * @details
 * @author Mars
 * @date 2023-10-22
 * @version V1.0.0
 * @copyright
 */
#ifndef _W25Q128_H_
#define _W25Q128_H_

#include <stdint.h>

//===============================================
int SPI_Flash_Read(int SPI, int addr, void *buf, int count);
void SPI_Flash_Write(int SPI, unsigned char *pBuffer, unsigned int WriteAddr, unsigned int NumByteToWrite);
int SPI_Flash_Erase_Block32K(int SPI, int addr);
int SPI_Flash_Erase_Block64K(int SPI, int addr);
int SPI_Flash_Read_UID(int SPI, unsigned char *rbuff);
unsigned char SPI_Flash_Read_Device_ID(int SPI);
//===============================================

void W25Q128_Demo(void);

#endif // W25Q128_H
