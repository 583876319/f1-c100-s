/**
 * @file main.c
 * @brief
 * @details
 * @author Mars
 * @date 2023-10-22
 * @version V1.0.0
 * @copyright
 */

#include "main.h"
#include "f1c100s.h"
#include "w25q128.h"

/*---------------------------------------------------
---- main
----------------------------------------------------*/
int main(void)
{
	Sys_Clock_Init(408000000);
	sys_mmu_init();
	sys_Uart0_Init(get_cpu_frequency(), 115200);
	sysprintf("\r\n\r\n\r\n");
	sysprintf("DSys_Start...\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);

	W25Q128_Demo(); // W25Q128测试

	while (1)
	{
	}
}

// End main.c
