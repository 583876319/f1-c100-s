#ifndef __SYS_SPI_H__
#define __SYS_SPI_H__

#define SPI0 0 // 0x01C05000
#define SPI1 1 // 0x01C06000

void sys_spi_init(int SPI);
void sys_spi_select(int SPI);
void sys_spi_deselect(int SPI);
int sys_spi_transfer(int SPI, void *txbuf, void *rxbuf, int len);
void sys_spi_set_max_clk(int clk);

int sys_spi_write_then_write(int SPI, void *txbuf0, int txlen0, void *txbuf1, int txlen1);
int sys_spi_write_then_read(int SPI, void *txbuf, int txlen, void *rxbuf, int rxlen);

#endif
