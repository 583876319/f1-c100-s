#include "sys_uart.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_lcd.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_timer.h"
#include <string.h>
#include <stdio.h>
#include <sys_mmu.h>
#include "stdlib.h"
//-----------------------
#include "sys_lcd_conf.h"
//-----------------------
#include "Test_png.h"

unsigned short *BT1; //
__align(1024) unsigned int LCDbuff1[XSIZE_PHYS * YSIZE_PHYS];
__align(1024) unsigned int LCDbuff2[XSIZE_PHYS * YSIZE_PHYS];

/*---------------------------------------------------
---- main
---- main
	1.图层0显示背景色-图层为ARGB32格式
	2.图层1显示PNG图片-图层为ARGB32格式

图片转换方法：
	1.然后使用【...\F1C100S_keil开发文件\开发与调试软件\Bin2C.exe】转换成C文件，加入到工程就可以了。
	2.图片数据需要4位对齐,不然一些图片会显示不正常，  __align(4)。
----------------------------------------------------*/
int main(void)
{
	unsigned int CPU_Frequency_hz = 408 * 1000000; // HZ
	Sys_Clock_Init(CPU_Frequency_hz);
	sys_mmu_init();
	sys_Uart0_Init(CPU_Frequency_hz, 115200);
	sysprintf("\r\n\r\n\r\n");
	sysprintf("Sys_Start...\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);
	/*定时器初始化*/
	Timer1_Init();
	
	GPIO_Congif(GPIOE, GPIO_Pin_5, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);
	GPIO_Congif(GPIOE, GPIO_Pin_6, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);
	GPIO_Congif(GPIOE, GPIO_Pin_11, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);

	GPIO_SET(GPIOE, GPIO_Pin_5);
	GPIO_SET(GPIOE, GPIO_Pin_6);
	GPIO_SET(GPIOE, GPIO_Pin_11);
	
	/*LCD 初始化*/
	BT1 = (unsigned short *)LCDbuff1;
	Sys_LCD_Init(XSIZE_PHYS, YSIZE_PHYS, (unsigned int *)LCDbuff1, (unsigned int *)LCDbuff2);
	Set_Layer_format(0, 0xA, 5); // 设置层0为ARGB8888
	/*测试PNG*/
	Test_png();
	/*测试返回*/
	while (1)
	{
		sysprintf("RUN\r\n");
		delay_ms(1000);
	}
}
