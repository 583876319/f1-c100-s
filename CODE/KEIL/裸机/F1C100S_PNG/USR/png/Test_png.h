﻿#ifndef _JPEG_TYPE_H_
#define _JPEG_TYPE_H_

#include "png.h"
#include "zlib.h"

// 自定义输入数据结构体
typedef struct
{
	unsigned char *image_data; // 图像存储地址
	unsigned int image_size;   // 图像尺寸
	unsigned int image_offset; // 图像偏移
} ImageSource;

typedef struct
{
	png_uint_32 width;		   // 图片宽度
	png_uint_32 height;		   // 图片高度
	png_color_16p pBackground; // 图片背景色
	int bit_depth;			   // 位深度
	int color_type;			   // 颜色类型
	int alpha_flag;			   // 是否有透明通道
	int channels;			   // 通道数
	double gamma;
	int size;			 // 解码后数据大小
	unsigned char *ARGB; // 存储颜色的地址
} Image_inf;

int Png_Decode(unsigned char *image_data, unsigned int dataSize);
void Test_png(void);

#endif
