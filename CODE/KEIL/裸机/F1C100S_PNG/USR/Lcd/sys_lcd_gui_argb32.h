/**
 * @file sys_lcd_gui_argb32.h
 * @brief
 * @details
 * @author Mars
 * @date 2023-10-22
 * @version V1.0.0
 * @copyright
 */
#ifndef _SYS_LCD_GUI_ARGB32_H_
#define _SYS_LCD_GUI_ARGB32_H_

void LCD_Draw_Points_ARGB32(int x, int y, unsigned int Color);

void LCD_Clear_ARGB32(unsigned int Color);
void LCD_Draw_ASCII_ARGB32(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void LCD_Draw_String_ARGB32(int x, int y, char *str, unsigned int Color1, unsigned int Color2, int fx, int fd);
void LCD_Draw_Picture_ARGB32(int x, int y, int w, int h, char *pic);
void LCD_Draw_HZ1616_ARGB32(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void LCD_Draw_HZ1616_ARGB32(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void LCD_Draw_HZ2424_ARGB32(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void LCD_Draw_HZ3232_ARGB32(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void LCD_Draw_HZ4040_ARGB32(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void LCD_Draw_HZ4848_ARGB32(int x, int y, int n, unsigned int Color1, unsigned int Color2, int fx, int fd);
void LCD_Move_Draw_Picture_ARGB32(int x, int y, int w, int h, char *pic);
int LCD_ReadTF_Draw_Picture_ARGB32(int x, int y, char *path);
void LCD_Draw_Rectangle_ARGB32(int x, int y, int w, int h, unsigned int Color);
void LCD_Draw_Picture24_ARGB32(int x, int y, int w, int h, char *pic);
void LCD_Draw_Picture32_ARGB32(int x, int y, int w, int h, unsigned char *pic);

#endif // SYS_LCD_GUI_ARGB32_H
