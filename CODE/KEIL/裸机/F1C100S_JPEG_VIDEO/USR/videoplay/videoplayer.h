#ifndef __VIDEOPLAYER_H
#define __VIDEOPLAYER_H

#include "stdio.h"
#include "string.h"
#include "avi.h"
#include "ff.h"
#include "sys_uart.h"
#include "sys_gpio.h"
#include "sys_dma.h"
#include "jpegdec_main.h"
#include "sys_timer.h"
#include "sys_audio.h"
#include "sys_lcd.h"
#include "f1c100s_malloc.h"
#include "sys_lcd.h"
//-----------------------
#include "sys_lcd_conf.h"
//-----------------------
#include "jpeg_type.h"

/*--使用OS-------*/
#define AVI_PLAY_OS                         1
/*--显示器-------*/
#define VIDEO_LCD_W                         XSIZE_PHYS
#define VIDEO_LCD_H                         YSIZE_PHYS
/*--JPEG-------*/
#define vedio_MACC_Init()                   MACC_Init(MACC_MODULE_MPEG_DEC)
#define vedio_MACC_Exit()                   MACC_Exit()
#define vedio_Jpeg_decoder_video(Stream,    StreamSize, Width, Height, buff_y, buff_c, _jpeg_format) \
	Jpeg_decoder_video(Stream, StreamSize, Width, Height, buff_y, buff_c, _jpeg_format, 100)
/*--malloc-------使用系统时都要使用f1c100s_malloc*/
#define vedio_malloc(size)                  avi_malloc(size)
#define vedio_free(pvoid)                   avi_free(pvoid)
#define vedio_memset(s,                     c, size) memset(s, c, size)
// 对齐分配
#define video_pmalloc                       f1c100s_malloc_Align
#define video_pfree                         f1c100s_free
/*--文件-------*/
#define video_f_open(fp,                    path, mode) f_open(fp, path, mode)
#define video_f_read(fp,                    buff, btr, br) f_read(fp, buff, btr, br)
#define video_f_lseek(fp,                   ots) f_lseek(fp, ots)
#define video_f_close(fp)                   f_close(fp)
/*--音频-------*/
#define vedio_WAV_Video_Play_Init(Channels, SampleRate) WAV_Video_Play_Init(Channels, SampleRate)
#define video_WAV_Video_Play_close()        WAV_Video_Play_close()
/*--Timer定时器-------*/
void Sys_video_Init(int time_us);
void video_timer_close(void);
#define video_Sys_video_Init(time_us)       Sys_video_Init(time_us)
#define video_video_timer_close()           video_timer_close()
/*--AVS定时器-------*/
// #define AVS_TIME	 //使用AVS定时器
#define AVS_TIME_n                          AVS_TIME_0
#define video_AVS_Time_Init(a)              AVS_Time_Init(AVS_TIME_n, a)
#define video_AVS_Time_Start()              AVS_Time_Start(AVS_TIME_n)
#define video_AVS_Time_Read()               AVS_Time_Read(AVS_TIME_n)
#define video_AVS_Time_Stop()               AVS_Time_Stop(AVS_TIME_n)

/*--LCD-------*/
#define video_set_video_window(x,           y, w, h) set_video_window(x, y, w, h)
/*--打印-------*/
#if 1
#define video_sysprintf                     sysprintf
#else
#define video_sysprintf                     (...)
#endif
/*--缓存大小定义-------*/
#define AVI_AUDIO_BUF_SIZE                  1024 * 5 // 定义avi解码时,音频buf大小.
#define AVI_VIDEO_BUF_SIZE                  1024 * 512 // 定义avi解码时,视频buf大小.
/*--定义按键-----------*/
#define nExit                               GPIO_READ(GPIOE, GPIO_Pin_3)
/*--定义视频通道开关-----------*/
#define __Enable_Layer_Video()              Enable_Layer_Video()
#define __Disable_Layer_Video()             Disable_Layer_Video()

// 播放一个mjpeg文件
// pname:文件名
// 返回值:
// KEY0_PRES:下一曲
// KEY1_PRES:上一曲
// 其他:错误
int video_play_avi_mjpeg(int x, int y, int w, int h, u8 *pname);

#endif
