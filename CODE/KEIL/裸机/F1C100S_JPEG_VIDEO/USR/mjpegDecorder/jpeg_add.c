#include "sys_uart.h"
#include "sys_clock.h"
#include "jpeg_type.h"
#include "sys_delay.h"
#include "stdlib.h"
#include "string.h"
#include "f1c100s_malloc.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_defe.h"
#include "sys_delay.h"

#define JPEG420 0
#define JPEG411 1
#define JPEG422 2
#define JPEG444 3
#define JPEG422T 4
#define JPEG400 5
#define JPEGERR 6

void eLIBs_CleanDCacheRegion(void *adr, __u32 bytes)
{
	// sysInvalidCache();
	sysFlushCache(D_CACHE);
}
void *eLIBs_memset(void *addr, int value, size_t size)
{
	return memset(addr, value, size);
}
void *esMEMS_Palloc(int a, int b)
{
	return f1c100s_malloc_Align(a, 1024);
}
int esMEMS_Pfree(void *a, int b)
{
	f1c100s_free(a);
	return 0;
}
void *esMEMS_Malloc(int a, int b)
{
	return f1c100s_malloc(b);
}
__u32 esMEMS_VA2PA(__u32 addr)
{
	return addr;
}

/* 中断 */
__s32 esINT_InsISR(__s32 irqno, __pISR_t pIsr, void *pArg)
{
	__IRQ_Init(IRQ_LEVEL_1, IRQ_VE, pIsr, 3, 0);
	return 0;
};
__s32 esINT_UniISR(__s32 irqno, __pISR_t pIsr)
{
	IRQ_Disable(IRQ_VE);
	return 0;
};
__s32 esINT_DisableINT(__s32 irqno)
{
	IRQ_Disable(IRQ_VE);
	return 0;
};
__s32 esINT_EnableINT(__s32 irqno)
{
	IRQ_Enable(IRQ_VE);
	return 0;
};

// 打印数组
void sprintf_buf(char *name, unsigned char *buff, int len)
{
	int i;
	sysprintf("%s\r\n", name);
	for (i = 0; i < len; i += 1)
	{
		if ((i > 0) && (i % 16 == 0))
			sysprintf("\r\n");
		if (i % 16 == 0)
			sysprintf("[%04X] ", i);
		sysprintf("%02X,", buff[i]);
	}
	sysprintf("\r\n");
}

/*--------------------------------------------------------------
硬转MB32-YUV格式到ARGB8888格式
yuv_format YUV输入格式
b_y,b_c, 输入YC缓存
in_w,in_h, 输入YC缓存宽高
bmp, 输出ARGB缓存
out_w,out_h 输出ARGB缓存宽高
--------------------------------------------------------------*/
int hardware_yuv_to_argb8888(int yuv_format, u8 *b_y, u8 *b_c, int in_w, int in_h, u8 *bmp, int out_w, int out_h)
{
	int defe_in_format;

	if (yuv_format == JPEG420)
	{
		defe_in_format = DE_SCAL_INYUV420;
	}
	else if ((yuv_format == JPEG422) || (yuv_format == JPEG444)) // 解码模块YUV444输出的YUV422
	{
		defe_in_format = DE_SCAL_INYUV422;
	}
	else if (yuv_format == JPEG411)
	{
		defe_in_format = DE_SCAL_INYUV411;
	}
	else
	{
		return -1;
	}
	// 开始defe
	Defe_Config_yuv_to_argb(defe_in_format, in_w, in_h, out_w, out_h, b_y, b_c, bmp);
	return 0;
}

/*--------------------------------------------------------------
macc打印-这里可以屏蔽掉打印输出
--------------------------------------------------------------*/
void macc_printf(char *pcStr, ...)
{
	char *argP;
	sysprintf(pcStr, argP);
}
/*--------------------------------------------------------------
//硬件解码时的空闲函数:
	1-纯裸机时函数里调用延时(1ms)或处理其它事情;
	2-OS系统调用时加入系统延时函数完成任务切换-ms级任务;
--------------------------------------------------------------*/
void Jpeg_Dec_Other_tasks(void)
{
	delay_ms(1);
}
