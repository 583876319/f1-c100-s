#ifndef	_JPEGDEC_TEST_H_
#define	_JPEGDEC_TEST_H_
#include "jpeg_type.h"

/*--------------------------------------------------------------
//计算Y,C缓存长度
--------------------------------------------------------------*/
#define get_y_len(w,h) (((w-1)/32*1024+(w-1)%32) + ((h-1)/32*(((w-1)/32+1)*1024)+(h-1)%32*32))
#define get_c_len(w,h) ((((h-1)/2/2*2)/32*(((w-1)/32+1)*1024)+((h-1)/2/2*2)%32*32) + (((w-1)/32*1024+(w-1)%32)/2*2))
#define get_yc_len(w,h) (((w-1)/32*1024+(w-1)%32) + ((h-1)/32*(((w-1)/32+1)*1024)+(h-1)%32*32))

typedef  void *__pmalloc(unsigned int __size,unsigned int align);
typedef  void __free(void * buff);

//void bmp_yuv420_to_argb8888(unsigned char* b_y,unsigned char* b_c,unsigned char* bmp,int w,int h);

/*--------------------------------------------------------------
内部使用malloc分配内存
支持YUV420,YUV411,YUV422,YUV444压缩格式
in_bmp 输入JPEG图片缓存，FF D8开始，FF D9结束
in_bmp_size 输入JPEG图片缓存大小
raw_w 返回JPEG图片实际宽度
raw_h 返回JPEG图片实际高度
out_argb argb图像输出缓存-可缩放【argb内存使用完后需要free释放】
out_argb_w argb图像缩放后输出宽度(0为输出原始宽度)
out_argb_h argb图像缩放后输出高度(0为输出原始高度)
_pmalloc 内存分配函数-块分配-字节对齐
_pfree 内存释放函数
dec_timeout 解码超时时间 = dec_timeout x Other_tasks()
--------------------------------------------------------------*/
int Jpeg_decoder(unsigned char* in_bmp,int in_bmp_size,int* raw_w,int* raw_h,
								 unsigned char** out_argb,int out_argb_w,int out_argb_h,
		             __pmalloc _pmalloc,__free _pfree,int dec_timeout);
/*--------------------------------------------------------------
外部定义内存
支持YUV420,YUV411,YUV422,YUV444压缩格式
in_bmp 输入JPEG图片缓存，FF D8开始，FF D9结束
in_bmp_size 输入JPEG图片缓存大小
raw_w 返回JPEG图片实际宽度
raw_h 返回JPEG图片实际高度
out_argb argb图像输出缓存-可缩放 1024对齐
out_argb_w argb图像缩放后输出宽度(0为输出原始宽度)
out_argb_h argb图像缩放后输出高度(0为输出原始高度)
b_y 亮度缓存 1024对齐
b_c 色度缓存 1024对齐
dec_timeout 解码超时时间 = dec_timeout x Other_tasks()
--------------------------------------------------------------*/
int Jpeg_decoder_2(unsigned char* in_bmp,int in_bmp_size,int* raw_w,int* raw_h,
								 unsigned char* out_argb,int out_argb_w,int out_argb_h,
		             unsigned char* b_y,unsigned char* b_c,int dec_timeout);

/*--------------------------------------------------------------
外部定义内存_JPEG视频解码
支持YUV420,YUV411,YUV422,YUV444压缩格式
in_bmp 输入JPEG图片缓存，FF D8开始，FF D9结束
in_bmp_size 输入JPEG图片缓存大小
out_w mb32_yuv图像输出宽度
out_h mb32_yuv图像输出高度
b_y 亮度缓存 1024对齐
b_c 色度缓存 1024对齐
jpeg_format 输出JPEG格式
dec_timeout 解码超时时间 = dec_timeout x Other_tasks()
--------------------------------------------------------------*/
int Jpeg_decoder_video(unsigned char* in_bmp,int in_bmp_size,
								 int* yuv_w,int* yuv_h,
		             unsigned char* b_y,unsigned char* b_c,int *jpeg_format,int dec_timeout);

/*--------------------------------------------------------------
返回JPEG高度宽度[会占用1-3ms时间]
in_bmp 输入JPEG图片缓存，FF D8开始，FF D9结束
in_bmp_size 输入JPEG图片缓存大小
jwidth 	返回图片宽度
jheight 返回图片高度
return 0成功 -1失败
--------------------------------------------------------------*/
int Get_Jpeg_width_height(unsigned char* in_bmp,int in_bmp_size,int *jwidth,int *jheight);
#endif





