#ifndef _JPEG_TYPE_H_
#define _JPEG_TYPE_H_

#include "sys_defe.h"
#include "sys_delay.h"
#include "sys_clock.h"
#include "sys_uart.h"
#include "sys_defe.h"
#include "string.h"

#define __u64                     unsigned long long
#define __u32                     unsigned int
#define __u16                     unsigned short
#define __u8                      unsigned char

#define __s64                     long long
#define __s32                     int
#define __s16                     short
#define __s8                      char

#define __bool                    BOOL
typedef void *__hdle;
typedef __s32 (*__pCB_ClkCtl_t)(__u32 /*cmd*/, __s32 /*aux*/);

// define sub-module of video engine
typedef enum __MACC_MOD_TYPE
{
    MACC_MODULE_MPEG_DEC = 0,
    MACC_MODULE_H264_DEC = 1,
    MACC_MODULE_VC1_DEC = 2,
    MACC_MODULE_RMVB_DEC = 3,
    MACC_MODULE_MPEG_ENC = 8,
    MACC_MODULE_ISP_CORE = 10,
    MACC_MODULE_JPEG = 11,

} __macc_mod_type_e;

#define EPDK_TRUE                 1
#define EPDK_FALSE                0
#define EPDK_OK                   0
#define EPDK_FAIL                 -1
#define OS_NO_ERR                 0u

#ifndef NULL
#define NULL                      0u
#endif

//----------------------------------------------
#define __krnl_event_t            void *

//----------------------------------------------
extern __u32 MACC_REGS_BASE;
#define H264ENCBASEADDR           (MACC_REGS_BASE + 0xB00)
#define H264ENC_INT_ENABLE_REG    (H264ENCBASEADDR + 0x14)
#define ISPBASEADDR               (MACC_REGS_BASE + 0xa00)
#define ISP_CONTROL_PARA_REG      (ISPBASEADDR + 0x08)

typedef __bool (*__pISR_t)(void *);

//----------------------------------------------DEFE
#define Video_Defe_Init           Defe_Init
#define Video_Defe_Exit           Defe_Exit
#define Defe_yuv420_to_argb_video Defe_Config_yuv420_to_argb_video
//----------------------------------------------定义时钟开打
#define mpeg_Open_Dev_Clock       Open_Dev_Clock
#define mpeg_Close_Dev_Clock      Close_Dev_Clock
//----------------------------------------------定义延时
void Jpeg_Dec_Other_tasks(void);
#define mpeg_delay_1ms            Jpeg_Dec_Other_tasks                // 1ms延时定义
//----------------------------------------------设置打印
void macc_printf(char *pcStr, ...);
#define m_sysprintf               macc_printf
//-------------------------------
// #define eLIBs_printf(...)
// #define __msg(...)
// #define __wrn(...)
#define eDbug(...)
// #define __inf(...)
//--------------------------------
#define eLIBs_printf              macc_printf
#define __msg                     macc_printf
#define __wrn                     macc_printf
// #define eDbug macc_printf
#define __inf                     macc_printf
//----------------------------------------------读写寄存器
#define readVeReg(n)              (*((volatile __u32 *)(n)))          /* word input */
#define writeVeReg(n,             c) (*((volatile __u32 *)(n)) = (c)) /* word output */

void *eLIBs_memset(void *addr, int value, size_t size);
void *esMEMS_Palloc(int a, int b);
void *esMEMS_Malloc(int a, int b);
int esMEMS_Pfree(void *a, int b);
__u32 esMEMS_VA2PA(__u32 addr);
__s32 esINT_InsISR(__s32 irqno, __pISR_t pIsr, void *pArg);
__s32 esINT_UniISR(__s32 irqno, __pISR_t pIsr);
__s32 esINT_DisableINT(__s32 irqno);
__s32 esINT_EnableINT(__s32 irqno);
void sprintf_buf(char *name, unsigned char *buff, int len);
void eLIBs_CleanDCacheRegion(void *adr, __u32 bytes);
int hardware_yuv_to_argb8888(int yuv_format, u8 *b_y, u8 *b_c, int in_w, int in_h, u8 *bmp, int out_w, int out_h);
void hardware_bmp_yuv420_to_argb8888_video(u8 *b_y, u8 *b_c, int in_w, int in_h, int out_w, int out_h);
//-----------------------
__s32 MACC_Init(__macc_mod_type_e nModType);
void MACC_Exit(void);

#endif
