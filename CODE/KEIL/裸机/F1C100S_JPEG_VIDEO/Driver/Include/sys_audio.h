#ifndef __SYS_AUDIO_H__
#define __SYS_AUDIO_H__

#include "sys_types.h"
#ifdef AUDIO_FILE_MODE_FATFS	
	#include "ff.h"
	#define aFIL FIL
#else
	#define aFIL void*
#endif

//------------------------------
#define MICIN 	            (1<<5)
#define FMINL             	(1<<4)
#define FMINR             	(1<<3)
#define LINEIN 							(1<<2)
#define Left_output_mixer 	(1<<1)
#define Right_output_mixer 	(1<<0)

//------------------------------
#define OUT_MODE_DAC 0 
#define OUT_MODE_IIS 1 

//------------------------------
#define FILE_MODE_FATFS    0 //文件系统模式
#define FILE_MODE_BUFF     1 //缓存模式
	
struct wav_
{
  u16 audio_format;//格式
  u16	num_channels;//通道数
  u32	format_blok_lenght;//格式块长度
	u32 sample_rate;//采集频率
	u32 byte_rate;//数据传输率
	u16 bits_per_sample;//采样位宽度
	u32 data_size;//数据区大小
	u32 play_time_ms;//播放时长
	u32 play_time_s;
};
//DEMO
void AUDIO_PLAY_Demo(void);
//
void AUDIO_Init(int play_init,int record_init);	
void AUDIO_play_volume(int volume);	
//播放部分
void MP3WAVplay(char *path);
void MP3WAVplay_exit(void);


#ifdef MP3_EN
int _MP3_Play(int file_mode,char *path,char *File,int File_size,int out_mode);
#endif

#ifdef WAV_EN
int _WAV_Play(int file_mode,char *path,char *File,int File_size,int out_mode);
#endif

#ifdef RECORED_EN
void AUDIO_record_power_analog(int power);
void AUDIO_RECORD_CH_Init(int recored_ch);	
//录音部分
INT32 AudioWriteFile(aFIL* fp,char* szAsciiName,PUINT16 pu16BufAddr,UINT32 u32Length,UINT32 u32SampleRate);
INT32 AudioWriteFileHead(aFIL* fp,char* szAsciiName, UINT32 u32Length,UINT32 u32SampleRate,UINT32 num_channels);
INT32 AudioWriteFileData(aFIL* fp, UINT16* pu16BufAddr, UINT32 u32Length);
INT32 AudioWriteFileClose(aFIL* fp);
int AUDIO_RECORD_Init(struct wav_ * wav_f);	
void AUDIO_RECORD_Demo(void);
#endif

#ifdef AUDIO_ADC_EN//ADC部分
void AUDIO_ADC_MODE_Demo(void);
void AUDIO_ADC_MODE_Demo(void);
#endif

#ifdef VIDEO_AVI_PLAY_WAV_EN  //AVI_WAV_PLAY
int WAV_Video_Play_Init(int num_channels,int sample_rate);
void WAV_Video_Play_close(void );
#endif

#ifdef NES_PLAY_WAV_EN
int NES_WAV_Play_Init(void);
void NES_wav_play(unsigned char * wav_buff,int Size);
#endif	

#endif




