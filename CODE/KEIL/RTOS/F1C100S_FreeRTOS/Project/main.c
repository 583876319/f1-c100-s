#include "sys_uart.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_delay.h"
#include "sys_lcd.h"
#include "sys_cache.h"
#include "sys_interrupt.h"
#include "sys_timer.h"
#include <string.h>
#include <stdio.h>
#include <sys_mmu.h>
#include "sys_TPAdc.h"
#include "FreeRTOS.h"
#include "task.h"

//--------------
#include "sys_lcd_conf.h"
#include "sys_touch_conf.h"
//--------------
/*---------------------------------------------------
FreeRTOS移植到F1C100S说明：
1.portASM.S 中 vPreemptiveTick在freertos_vectors.s的IRQ中断被调用
	程序中判断中断类型，1【是Tick中断则进入Tick,需要切换任务时则进入上下文切换,最后清中断标志】 2【当不是Tick中断时则进入其它中断】
	xTaskIncrementTick 完成系统Tick
	vTaskSwitchContext 当有多个任务时完成上下文切换
2.portASM.S 中 vPortYieldProcessor在freertos_vectors.s的SWI软中断被调用
	vPortYield 进入SWI软中断时调用
	vPortYieldProcessor 软中断处理程序
	vPortStartFirstTask 进入第一任务【恢复上下文则进入第一任务】
3.ptrt.c 中 prvSetupTimerInterrupt 初始化Tick定时器
4.FreeRTOSConfig.h 中配置各种参数，包括系统堆栈大小等
----------------------------------------------------*/

/*---------------------------------------------------
LED任务1
----------------------------------------------------*/
static void LED1_Task(void *pvParameters)
{
	(void)pvParameters;
	while (1)
	{
		// GPIO_SET(GPIOE, GPIO_Pin_5);
		// vTaskDelay(500 / portTICK_PERIOD_MS);
		// GPIO_RESET(GPIOE, GPIO_Pin_5);
		vTaskDelay(500 / portTICK_PERIOD_MS);
		sysprintf("LED1_Task\r\n");
	}
}
/*---------------------------------------------------
LED任务2
----------------------------------------------------*/
static void LED2_Task(void *pvParameters)
{
	(void)pvParameters;
	while (1)
	{
		// GPIO_SET(GPIOE, GPIO_Pin_6);
		// vTaskDelay(250 / portTICK_PERIOD_MS);
		// GPIO_RESET(GPIOE, GPIO_Pin_6);
		vTaskDelay(250 / portTICK_PERIOD_MS);
		sysprintf("LED2_Task\r\n");
	}
}
/*---------------------------------------------------
打印任务
----------------------------------------------------*/
static void PRINT_Task(void *pvParameters)
{
	int Cn = 0;
	(void)pvParameters;
	sysprintf("RUN PRINT_Task\r\n");
	while (1)
	{
		Cn++;
		sysprintf("Cn:%02d\r\n", Cn);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}
/*---------------------------------------------------
主任务
----------------------------------------------------*/
static void MAIN_Task(void *pvParameters)
{
	(void)pvParameters;
	sysprintf("RUN MAIN_Task\r\n");

	xTaskCreate(LED1_Task, "LED1_Task", 1024, NULL, 3, NULL);
	xTaskCreate(LED2_Task, "LED2_Task", 1024, NULL, 3, NULL);
	xTaskCreate(PRINT_Task, "PRINT_Task", 1024, NULL, 3, NULL);
	while (1)
	{
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}

/*---------------------------------------------------
---- main
---- main
----------------------------------------------------*/
int main(void)
{
	Sys_Clock_Init(408000000);
	sys_mmu_init();
	Sys_Uart_Init(UART0, 408000000, 115200, 0);
	sysprintf("\r\n\r\n\r\n");
	sysprintf("FreeRTOS Start!!!\r\n");
	sysprintf("Dram Init Size %d Mb.\r\n", 32);
	// 开全局中断
	sysSetLocalInterrupt(ENABLE_IRQ);
	// 创建任务
	xTaskCreate(MAIN_Task, "MAIN_Task", 1024, NULL, 3, NULL);
	// 启动任务
	vTaskStartScheduler();
	return 0;
}
